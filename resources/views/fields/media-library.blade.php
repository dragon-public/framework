<input readonly placeholder="Click to select images" type="text" value="{{ $value }}" id="{{ $html_id }}" name="{{ $html_id }}" />
<script type="text/javascript">
jQuery(document).ready(() => {
	dragonMedia.displayMediaLibrary('{{ $html_id }}', '{{ $multiple }}');
});
</script>
