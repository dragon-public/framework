<input id="{{ $id }}" name="{{ $name }}" />
<script type="text/javascript">
jQuery(document).ready(() => {
	new Tokenfield({
		el: document.querySelector('#{{ $id }}'),
		items: {!! $items !!},
		setItems: {!! $value !!},
		newItems: {{ $new_items }},
		itemName: '{{ $name }}'
	});
});
</script>
