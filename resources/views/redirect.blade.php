<div class="card card-full-width">
	<table class="form-table">
		<tr>
			<td>
				{{ $display_text }}<br />
				<a class="button button-primary" href="{!! $next !!}">
					Go Here to Continue Your Journey
				</a>
			</td>
		</tr>
	</table>
</div>
