<div class="tablenav top tabletop">
	<form class="search-table" method="get">
		@foreach ($input->get() as $key => $val)
			<input type="hidden" name="{{ $key }}" value="{{ $val }}" />
		@endforeach
		
		@if ($searchable)
			<p class="search-box">
				<label class="screen-reader-text" for="search-input">Search:</label>
				<input
					value="{{ $input->get('s') }}"
					type="search"
					id="search-input"
					class="wp-filter-search"
					name="s"
					placeholder="Search..."
				/>
				<input type="submit" id="search-submit" class="button" value="Search" />
			</p>
		@endif
		
		@if ($dateSeachable)
    		<br />
    		<br />
    		<p class="search-box">
    			<label class="screen-reader-text" for="search-input">Search by Date:</label>
    			From: <input
    					type="date"
    					class="wp-filter-search"
    					name="date-start"
    					value="{{ $input->get('date-start') }}"
    				/>
    			To: <input
    					type="date"
    					class="wp-filter-search"
    					name="date-end"
    					value="{{ $input->get('date-end') }}"
    				/>
    			<input type="submit" class="button" value="Search by Date" />
    		</p>
		@endif
		
		<p class="search-box-left">
		@if (!empty($filters))
			@foreach ($filters as $name => $data)
				<select name="{{ $name }}">
					<option value="">{{ $data['label'] }}</option>
					@foreach ($data['options'] as $value => $displayable)
						<option
							value="{{ $value }}"
							{{ $input->get($name) == $value ? 'selected' : '' }}
					    >{{ $displayable }}</option>
					@endforeach
				</select>
			@endforeach
			<input type="submit" class="button" value="Apply Filters">
		@endif
		</p>
	</form>
	<form method="POST">
	@foreach ($actionButtons as $buttonName => $value)
		<input type="submit" class="button" value="{{ $value }}" name="{{ $buttonName }}" />
	@endforeach
	</form>
	@foreach ($bulkActions as $actionName => $data)
		<form method="POST">
			<select name="{{ $actionName }}">
				@php
					$options = array_merge([null => $data['label']], $data['options']);
				@endphp
				@foreach ($options as $value => $displayable)
					<option value="{{ $value }}">{{ $displayable }}</option>';
				@endforeach
			</select>
			<input type="hidden" name="action_ids" class="action-ids" value="" />
			<input type="submit" class="button submit-action" value="Apply" />
		</form>
	@endforeach
	
	@include('table.paging')
</div>