@php
    use Dragon\Form\DropDown;
    use Dragon\Form\InputBox;
    use Dragon\Form\TextArea;
    use Dragon\User;
    use Dragon\Form\TokenField;
    use Dragon\Form\MediaLibrary;
    use Dragon\Form\Checkbox;
@endphp

@foreach ($sections as $sectionName => $fields)
	<tr>
		<td colspan="2">
			<h2>{{ $sectionName }}</h2>
		</td>
	</tr>
	@foreach ($fields as $fieldName => $fieldData)
		@php
    		if (empty($fieldData['attributes'])) {
    			$fieldData['attributes'] = [];
    		}
    		
    		$fieldData['column'] = $fieldName;
    		$fieldData['attributes'] = array_merge($fieldData['attributes'], [
    			'id'	=> $fieldName,
    			'name'	=> $fieldName,
    		]);
    		
    		if (!empty($userId)) {
    			$value = User::getMeta($fieldName, old($fieldName), false, (int)$userId);
    		} else {
    			$value = get_option($fieldName, old($fieldName));
    		}
    		
    		if (!empty($fieldData['encrypted'])) {
    			$value = dragonDecrypt($value);
    		}
		@endphp
		<tr class="form-field {{ !empty($fieldData['required']) ? 'form-required' : '' }}">
			<th scope="row">
				<label for="{{ $fieldName }}">
					{{ $fieldData['label'] }}
					@if (!empty($fieldData['required']))
						{{ ' (required)' }}
					@endif
				</label>
			</th>
			<td>
				@php
    				switch($fieldData['type']) {
    					case 'text':
    					case 'number':
    					case 'date':
    					case 'email':
    					case 'url':
    					case 'password':
    						$fieldData['attributes']['value'] = $value;
    						echo InputBox::create($fieldData);
    						break;
    					case 'textarea':
    						echo TextArea::create($fieldData, $value);
    						break;
    					case 'checkbox':
    						echo Checkbox::create($fieldData, $value);
    						break;
    					case 'select':
    						echo DropDown::create($fieldData, $value);
    						break;
    					case 'token':
    						echo TokenField::create($fieldData, $value);
    						break;
    					case 'media':
    						echo MediaLibrary::create($fieldData, $value);
    						break;
    					case 'wysiwyg':
    						if (!empty($model) && !empty($model[$dbColumn])) {
    							$value = $model[$dbColumn];
    						}
    						
    						$value = old($fieldName, null, false) ?? $value ?? '';
    						wp_editor($value, $fieldName);
    						break;
    					case 'callback':
    						if (!empty($fieldData['callback']) && is_callable($fieldData['callback'])) {
    							echo $fieldData['callback']($userId, $fieldData, $value);
    						}
    						break;
    				}
				@endphp
				
				@if (!empty($fieldData['description']))
					<p class="description">{{ $fieldData['description'] }}</p>
				@endif
			</td>
		</tr>
	@endforeach
@endforeach
