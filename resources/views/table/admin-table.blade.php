<div class="wrap">
	<h1 class="wp-heading-inline">
	{{ get_admin_page_title() }}
	@if (!empty($createLink))
		<a class="page-title-action" href="{!! $createLink !!}">
			<strong>Add New</strong>
		</a>
	@endif
	</h1>
	@if (!empty($pageBanner))
	    <div>@include($pageBanner)</div>
	@endif
	<p>{{ $pageText }}</p>
	@include('table.table-top')
	<table class="wp-list-table widefat fixed striped pages">
		<thead>
			<tr>
				@if (!empty($isSortable) && !empty($rows))
					<td scope="col" class="sorting-handle-column"></td>
				@endif
				
				@if (!empty($bulkActions) && !empty($rows))
					<td scope="col" class="check-column">
						<input type="checkbox" class="dragon-check-all" />
					</td>
				@endif
				
				@foreach ($headers as $header)
					<td scope="col">{{ $header }}</td>
				@endforeach
			</tr>
		</thead>
		
		<tbody
		@if (!empty($isSortable))
			id="dragon-drop"
		@endif
		>
			@foreach ($rows as $row)
			<tr data-id="{{ $row[0]['id'] }}">
				@if ($isSortable)
					<td class="sorting-handle-column">
						<span class="dashicons dashicons-menu dragon-drop-handle"></span>
					</td>
				@endif
				
				@if (!empty($bulkActions))
					<td>
						<input class="check-me" type="checkbox" name="rows[]" value="{{ $row[0]['id'] }}" />
					</td>
				@endif
				
				@foreach ($row as $column)
					
					@if (!empty($column['links']))
    					@php
    						$links = $column['links'];
    						$viewLink = empty($links['view']) ? null : $links['view'];
    						$editLink = empty($links['edit']) ? null : $links['edit'];
    						$defaultLink = empty($links['view']) ? $editLink : $viewLink;
						@endphp
						<td class="has-row-actions column-primary">
							<strong>
								@if (empty($defaultLink))
									{{ $column['data'] }}
								@else
									<a href="{!! $defaultLink['link'] !!}" class="row-title">
										{{ $column['data'] }}
									</a>
								@endif
							</strong>
							<div>
								@php
								    $i = 1;
								@endphp
								
								@foreach ($links as $linkName => $linkInfo)
									@php
										$class = empty($linkInfo['class']) ? $linkName : $linkInfo['class'];
									@endphp
									<span class="{{ $class }}">
										<a href="{!! $linkInfo['link'] !!}">
											@if (empty($linkInfo['name']))
												{{ ucwords($linkName) }}
											@else
												{{ $linkInfo['name'] }}
											@endif
										</a>
										@if ($i !== count($links))
											@if($multiLineLinks)
												<br />
											@else
												|
											@endif
										@endif
									</span>
									@php
										$i++;
									@endphp
									
								@endforeach
							</div>
						</td>
					@else
						<td>
							@if (is_string($column['data']))
								{!! nl2br($column['data']) !!}
							@else
								{{ $column['data'] }}
							@endif
						</td>
					@endif
					
				@endforeach
			</tr>
			@endforeach
			
			@if (empty($rows))
				<tr>
					<td colspan="{{ count($headers) }}">
						Nothing to Display
					</td>
				</tr>
			@endif
		</tbody>
		<tfoot>
			<tr>
				@if (!empty($isSortable) && !empty($rows))
					<td scope="col" class="sorting-handle-column"></td>
				@endif
				
				@if (!empty($bulkActions) && !empty($rows))
					<td scope="col" class="check-column">
						<input type="checkbox" class="dragon-check-all" />
					</td>
				@endif
				
				@foreach ($headers as $header)
					<th scope="col">{{ $header }}</th>
				@endforeach
			</tr>
		</tfoot>
	</table>
	@include('table.table-top')
	<div>
		<h1 class="wp-heading-inline">
		{{ get_admin_page_title() }}
		
		@if (!empty($createLink))
			<a class="page-title-action" href="{!! $createLink !!}">
				<strong>Add New</strong>
			</a>
		@endif
		</h1>
	</div>
</div>
