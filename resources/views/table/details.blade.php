@php
    use Dragon\Form\DropDown;
    use Dragon\Form\InputBox;
    use Dragon\Form\TextArea;
    use Dragon\Config;
    use Dragon\Form\EmailMessage;
    use Dragon\Form\TokenField;
    use Dragon\Form\MediaLibrary;
    use Dragon\Form\Checkbox;
    use Dragon\View;
    use Dragon\Blade;
    
    $prefix = Config::$namespace;
@endphp
<div class="card card-full-width">
	<div class="{{ $prefix }}-intro">	
		<h1>{{ $pageTitle }}</h1>
	</div>
	@if (!empty($pageBanner))
		<div>@include($pageBanner)</div>
	@endif
	
	@if (!empty($pageDescription))
		<p>{{ $pageDescription }}</p>
	@endif
	<form method="POST">
		<input name="{{ $prefix }}_nonce" value="{{ wp_create_nonce($prefix . '_nonce') }}" type="hidden" />
		@php
			$active = 'active';
			$displayBlock = 'style="display: block;"';
		@endphp
		
			@foreach ($sections as $sectionName => $fields)
				@if ($sectionName === '[ALL]')
					@php
						$fields = array_keys($fieldDescriptors);
					@endphp
				@else
					<button class="dragon-accordion {{ $active }}">{{ $sectionName }}</button>
					<div class="dragon-panel" {{ $displayBlock }}>
				@endif
				
				<table class="form-table">
				@foreach ($fields as $dbColumn)
					@php
    					$fieldData = $fieldDescriptors[$dbColumn];
    					$fieldName = 'details_' . $dbColumn;
    					$fieldData['value'] = empty($fieldData['value']) ? null : $fieldData['value'];
    					
    					$value = old($fieldName) ?? $fieldData['value'] ?? $model[$dbColumn] ?? '';
    					if (!empty($fieldData['valueCallback'])) {
    						$value = $fieldData['valueCallback']($value);
    					}
					@endphp
						<tr class="form-field {{ !empty($fieldData['required']) ? 'form-required' : '' }}">
							<th scope="row">
								<label for="{{ $fieldName; }}">
									{{ $fieldData['label'] }}
									@if (!empty($fieldData['required']))
										{{ ' (required)' }}
									@endif
								</label>
							</th>
							<td>
								@php
								if (empty($fieldData['attributes'])) {
									$fieldData['attributes'] = [];
								}
								
								$fieldData['column'] = $dbColumn;
								
								$fieldData['attributes'] = array_merge($fieldData['attributes'], [
									'id'	=> $fieldName,
									'name'	=> $fieldName,
								]);
								
								switch($fieldData['type']) {
									case 'text':
									case 'number':
									case 'date':
									case 'email':
									case 'url':
									case 'password':
										$fieldData['attributes']['value'] = $value;
										echo InputBox::create($fieldData, $readonly);
										break;
									case 'checkbox':
										echo Checkbox::create($fieldData, $value, $readonly);
										break;
									case 'textarea':
										$value = old($fieldName, null, false) ?? $fieldData['value'] ?? $model[$dbColumn] ?? '';
										echo TextArea::create($fieldData, $value, $readonly);
										break;
									case 'select':
										echo DropDown::create($fieldData, $value, $readonly);
										break;
									case 'email-message':
										if (!$readonly) {
											echo EmailMessage::create($fieldData);
										}
										break;
									case 'token':
										echo TokenField::create($fieldData, $value, $readonly);
										break;
									case 'media':
										if (!$readonly) {
											echo MediaLibrary::create($fieldData, $value);
										}
										break;
									case 'media-image':
									    echo Blade::view('fields.media-image', [
									       'id_field' => 'details_' . $fieldData['id_field'],
									       'media_size' => (empty($fieldData['media_size']) ? 'thumbnail' : $fieldData['media_size'])
									    ]);
									    break;
									case 'wysiwyg':
										$value = old($fieldName, null, false) ?? $model[$dbColumn] ?? '';
										if ($readonly) {
											echo $value;
										} else {
											wp_editor($value, $fieldName);
										}
										break;
									case 'readonly':
										echo $fieldData['display-text'];
										break;
								}
								@endphp
								
								@if (!empty($fieldData['description']))
									<p class="description">{{ $fieldData['description'] }}</p>
								@endif
							</td>
						</tr>
					@endforeach
					</table>
					@if ($sectionName !== '[ALL]')
						</div>
					@endif
				
				@php
					$active = null;
					$displayBlock = null;
				@endphp
			@endforeach
		<table class="form-table">
			<tr>
				@if (empty($goBack)) {
					<th></th>
				@else
					<th><a href="{!! $goBack !!}">Go Back</a></th>
				@endif

				@if (!$readonly)
					<td>
						<input name="{{ $prefix }}_save" type="submit" value="Save" class="button button-primary" />
					</td>
				@endif
			</tr>
		</table>
	</form>
</div>
