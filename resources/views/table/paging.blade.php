@if (empty($pagingData['pagingVar']))
	@php
		$pagingData['pagingVar'] = 'paged';
	@endphp
@endif
<div class="tablenav-pages">
	<span class="displaying-num">
	@php
		$total = $paging->getTotalPages($totalRows);
	@endphp
	
	{{ number_format($total) }} {{ ($total === 1 ? 'item' : 'items') }}
	</span>
	@if (!empty($pagingData['last_page']))
		<form method="GET" action="{!! $pagingData['path'] !!}">
			<span class='pagination-links'>
				@if ($pagingData['current_page'] > 1)
					<a class='first-page button' href='{!! $pagingData['first_page_url'] !!}'>&laquo;</a>
					<a class='prev-page button' href='{!! $pagingData['prev_page_url'] !!}'>&lsaquo;</a>
				@else
					<span class="tablenav-pages-navspan button disabled">&laquo;</span>
   					<span class="tablenav-pages-navspan button disabled">&lsaquo;</span>
   				@endif
				<span class="paging-input">
					<input
						class='current-page'
						id='current-page-selector'
						type='text'
						name='{{ $pagingData['pagingVar'] }}'
						value='{{ $pagingData['current_page'] }}'
						size='3'
					/>
					@foreach ($input->get() as $key => $val)
						@if ($key === 'paged')
							@continue
						@endif
						
						<input type="hidden" name="{{ $key }}" value="{{ $val }}" />
					@endforeach
					<span class='tablenav-paging-text'> of
						<span class='total-pages'>{{ $pagingData['last_page'] }}</span>
					</span>
				</span>
				@if ($pagingData['current_page'] !== $pagingData['last_page'])
					<a class='next-page button' href='{!! $pagingData['next_page_url'] !!}'>&rsaquo;</a>
					<a class='last-page button' href='{!! $pagingData['last_page_url'] !!}'>&raquo;</a>
				@else
					<span class="tablenav-pages-navspan button disabled">&rsaquo;</span>
   					<span class="tablenav-pages-navspan button disabled">&raquo;</span>
   				@endif
			</span>
		</form>
	@endif
</div>
