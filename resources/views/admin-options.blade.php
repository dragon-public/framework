@if(!empty($notice))
	{!! $notice !!}
@endif
<div class="card card-full-width">
	<div class="{{ $prefix }}-intro">	
		<h1>{{ $title }}</h1>
	</div>

	<form method="POST">
		<input name="{{ $prefix }}_nonce" value="{!! $nonce !!}" type="hidden" />
		<table class="form-table">
			<tr>
				<td>
					<input name="{{ $prefix }}_save_settings" type="submit" value="Save All" class="button button-primary" />
				</td>
			</tr>
			{!! $rows !!}
			<tr>
				<td>
					<input name="{{ $prefix }}_save_settings" type="submit" value="Save All" class="button button-primary" />
				</td>
			</tr>
		</table>
	</form>
</div>