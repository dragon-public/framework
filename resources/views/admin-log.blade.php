<div class="card card-full-width">
	<form method="POST">
		<table class="form-table">
			<tr>
				<td colspan="2">
					<h2>Log</h2>
				</td>
			</tr>
			<tr>
				<td>
					<input name="dragon_clear_log" type="submit" value="Clear Log" class="button button-primary" />
				</td>
			</tr>
			<tr>
				<td>
					<textarea cols="150" rows="40">{!! $log !!}</textarea>
				</td>
			</tr>
			<tr>
				<td>
					<input name="dragon_clear_log" type="submit" value="Clear Log" class="button button-primary" />
				</td>
			</tr>
		</table>
	</form>
</div>