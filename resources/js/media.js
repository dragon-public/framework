const dragonMedia = (function ($) {
	let started = false;
	
	function init() {
		if (started) {
			return;
		}
		
		started = true;
		displayMediaImage();
	}
	
	function displayMediaLibrary(fieldId, multiple = true) {
	    var image_frame;
	
	    $('#' + fieldId).click(function(e){
	        if (image_frame){
	           image_frame.open();
	           return;
	        }
	
	        var $that = $(this);
	        
	        image_frame = wp.media({
	            title: 'Select Gallery Images',
	            library : {
	                type : 'image'
	            },
	            multiple
	        });
	
	        image_frame.states.add([
	            new wp.media.controller.Library({
	                id:         fieldId + '-gallery',
	                title:      'Select Images',
	                priority:   20,
	                toolbar:    'main-gallery',
	                filterable: 'uploaded',
	                library:    wp.media.query( image_frame.options.library ),
	                multiple:   image_frame.options.multiple ? 'reset' : false,
	                editable:   true,
	                allowLocalEdits: true,
	                displaySettings: true,
	                displayUserSettings: true
	            })
	        ]);
	
	        image_frame.open();
	
	        image_frame.on( 'select', function() {
	
	            var ids = ''
	            var attachments_arr = image_frame.state().get('selection').toJSON();
	            $(attachments_arr).each(function(index){
	                sep = ( index != ( attachments_arr.length - 1 ) ) ? ',' : '';
	                ids += $(this)[0].id + sep;
	            });
	            $that.val(ids).change();
	
	        });
	
	    });
	}
	
	// Privates
	function displayMediaImage(isRecursing = false) {
		if ($('#dragon-media-image') == undefined || $('#dragon-media-image').attr('data-id-field') == "") {
			return;
		}
		
		let idField = $('#dragon-media-image').attr('data-id-field');
		if (!isRecursing) {
			$('#' + idField).change(() => {displayMediaImage(true)});
		}
		
		if (isInt($('#' + idField).val()) === false) {
			return;
		}
		
		let size = 'thumbnail';
		if ($('#dragon-media-image').attr('data-media-size') != undefined) {
			size = $('#dragon-media-image').attr('data-media-size');
		}
		
		$.post(ajax_url, {
			'action': 'dragon_get_media_image_url',
			'media_id': $('#' + idField).val(),
			'media_size': size,
		}, function(res) {
			response = JSON.parse(res);
			if (response.url.length > 0 && response.url != false) {
				let img = $('<img src="' + response.url + '" />');
				$('#dragon-media-image').html('').append(img);
			}
		});
	}
	
	function isInt(value) {
	  return !isNaN(value) && 
	         parseInt(Number(value)) == value && 
	         !isNaN(parseInt(value, 10));
	}
	
	return { init, displayMediaLibrary };
})(jQuery);

jQuery(document).ready(() => {
	dragonMedia.init();
});
