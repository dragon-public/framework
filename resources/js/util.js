/**
 * @author Red Scale Corporation
 */

const dragonUtil = (function ($) {
	let started = false;
	
	function init() {
		if (started) {
			return;
		}
		
		started = true;
		
		$('.dragon-check-all').click(event => {
			checkAll(event.target.checked);
		});
		
		$('.check-me').click(event => {
			checkOne(event.target);
		});
		
		handleAdminTableSorting();
		handleAccordion();
	}
	
	function getUrlParam(param) {
	    const url = window.location.search.substring(1);
	    const urlVars = url.split('&');
	    let paramName = null;
	
	    for (let i = 0; i < urlVars.length; i++) {
	        paramName = urlVars[i].split('=');
	
	        if (paramName[0] === param) {
	            return paramName[1] === undefined ? true : decodeURIComponent(paramName[1]);
	        }
	    }

		return null;
	};
	
	// Private
	
	function handleAccordion() {
		var acc = document.getElementsByClassName("dragon-accordion");
		var i;
		
		for (i = 0; i < acc.length; i++) {
		  acc[i].addEventListener("click", function(event) {
			event.preventDefault();
		    this.classList.toggle("active");
		    var panel = this.nextElementSibling;
		    if (panel.style.display === "block") {
		      panel.style.display = "none";
		    } else {
		      panel.style.display = "block";
		    }
		  });
		}
	}
	
	function handleAdminTableSorting() {
		let el = document.getElementById('dragon-drop');
		if (el == null) {
			return;
		}
		
		let sortable = Sortable.create(el, {
			handle: '.dragon-drop-handle',
			animation: 150,
			onUpdate: event => {
				let data = {
					'action': 'dragon_set_table_sorting',
					'order': sortable.toArray(),
					'page_slug': getUrlParam('page'),
				};
				
				$.post(ajax_url, data);
			}
		});
	}
	
	function checkAll(shouldCheck) {
		let ids = [];
		$('.check-me').each((index, element) => {
			if (shouldCheck === false) {
				element.checked = false;
			} else {
				element.checked = true;
				ids[ids.length] = $(element).val();
			}
		});
		$('.action-ids').val(ids.join());
	}
	
	function checkOne(element) {
		let existingIds = $('.action-ids').val();
		let ids = existingIds.length === 0 ? [] : $('.action-ids').val().split(',');
		let currentId = $(element).val();
		
		if (element.checked) {
			ids[ids.length] = currentId;
		} else {
			let index = ids.indexOf(currentId);
			if (index > -1) {
				ids.splice(index, 1);
			}
		}
		
		$('.action-ids').val(ids.join());
	}
	
	return { init, getUrlParam };
})(jQuery);

jQuery(document).ready(() => {
	dragonUtil.init();
});
