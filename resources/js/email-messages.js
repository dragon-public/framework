const dragonEmailMessages = (($) => {
	function init () {
		const emailMessageFrame = $('#dragon-email-message');
		if (emailMessageFrame.length === 0) {
			return;
		}

		const messageType = $('#details_type').val();
		var data = {
			'action': 'dragon_get_email_template',
			'email_template': messageType,
			'email_id': getParam('id'),
		};

		$.post(ajax_url, data, function(res) {
			response = JSON.parse(res);
			if (response.template.length > 0) {
				emailMessageFrame.html(response.template);
			}
		});
	}
	
	// Privates
	
	function getParam(name) {
		const urlParams = new URLSearchParams(window.location.search);
		return urlParams.get(name);
	}

	return { init };
})(jQuery);

jQuery(document).ready(() => {
	dragonEmailMessages.init();
});
