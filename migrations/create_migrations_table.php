<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Capsule\Manager as Capsule;
use Dragon\Abstracts\MigrationAbstract;

class CreateMigrationsTable extends MigrationAbstract {
	public $tableName = 'dragon_migrations';
	
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Capsule::schema()->create($this->tableName, function (Blueprint $table) {
    		
    		$table->bigIncrements('id');
    		$table->string('migration');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Capsule::schema()->drop($this->tableName);
    }
}
