<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Capsule\Manager as Capsule;
use Dragon\Abstracts\MigrationAbstract;

class CreatePseudoSessionsTable extends MigrationAbstract {
	public $tableName = 'dragon_pseudo_sessions';
	
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Capsule::schema()->create($this->tableName, function (Blueprint $table) {
    		
    		$table->bigIncrements('id');
    		$table->string('token');
    		$table->string('key');
    		$table->string('value');
    		$table->timestamp('expiry');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Capsule::schema()->drop($this->tableName);
    }
}
