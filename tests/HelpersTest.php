<?php

/*
 * Missing tests for...
 * old() - Needs to mock $_POST
 * dragonAssets() - Relies on WP functionality that fucks up the path only in tests
 */

class HelpersTest extends WP_UnitTestCase {
	public function testCanEncryptAndDecrypt() {
		$test = 'test';
		$encryped = dragonEncrypt($test);
		$this->assertSame($test, dragonDecrypt($encryped));
	}
	
	public function testCanMakeACamelBeAloof() {
		$kebab = 'im-a-camel';
		$expected = 'ImACamel';
		$camel = kababToCamel($kebab);
		$this->assertSame($expected, $camel);
	}
	
	public function testCanSedateACamel() {
		$kebab = 'im-a-camel';
		$expected = 'imACamel';
		$camel = kababToCamel($kebab, true);
		$this->assertSame($expected, $camel);
	}
	
	public function testThatICanRetitleMyCamel() {
		$camel = "retitleMe";
		$retitled = camelTitle($camel);
		$this->assertSame('Retitle Me', $retitled);
	}
	
	public function testCanGetArrayItemUsingDotNotation() {
		$array = [
			'test' => [
				'nested' => 'item',
			],
		];
		
		$out = arrayDot($array, 'test.nested');
		$this->assertSame('item', $out);
		
		$out = arrayDot($array, 'test.missing');
		$this->assertSame(null, $out);
		
		$out = arrayDot($array, 'test.missing', false);
		$this->assertSame(false, $out);
	}
	
	public function testCanGetLanguageItem() {
		$out = lang('en.validation', 'test');
		$this->assertSame('For unit tests', $out);
		
		$out = lang('en.missing', 'missing');
		$this->assertSame(null, $out);
		
		$out = lang('en.missing', 'missing', 'blah');
		$this->assertSame('blah', $out);
	}
}
