<?php

use Dragon\Config;

class ConfigTest extends WP_UnitTestCase {
	public function testCanGetAndSetItems() {
		Config::open();
		Config::set('test', 'here');
		Config::save();
		$this->assertSame('here', Config::get('test'));
	}
}
