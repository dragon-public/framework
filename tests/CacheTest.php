<?php

use Dragon\Cache;

class CacheTest extends WP_UnitTestCase {
	public function testCanGetAndSetEntries() {
		Cache::set('test', 'here');
		$this->assertSame('here', Cache::get('test'));
	}
	
	public function testCanDeleteCachedEntries() {
		Cache::set('testdelete', 'here');
		Cache::delete('testdelete');
		$this->assertSame(null, Cache::get('testdelete'));
	}
}
