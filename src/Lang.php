<?php

namespace Dragon;

use Dragon\Abstracts\ArrayDotFileAbstract;

class Lang extends ArrayDotFileAbstract {
    protected static function getFileBasePath() {
        return FileSystem::getLanguagePath();
    }
}
