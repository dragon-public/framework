<?php

namespace Dragon;

use Illuminate\Container\Container;
use Illuminate\Config\Repository;
use Illuminate\Filesystem\FilesystemManager;
use Illuminate\Contracts\Filesystem\Filesystem;

/**
 * @method static array allDirectories(string|null $directory = null)
 * @method static array allFiles(string|null $directory = null)
 * @method static array directories(string|null $directory = null, bool $recursive = false)
 * @method static array files(string|null $directory = null, bool $recursive = false)
 * @method static bool append(string $path, string $data)
 * @method static bool copy(string $from, string $to)
 * @method static bool delete(string|array $paths)
 * @method static bool deleteDirectory(string $directory)
 * @method static bool exists(string $path)
 * @method static bool makeDirectory(string $path)
 * @method static bool missing(string $path)
 * @method static bool move(string $from, string $to)
 * @method static bool prepend(string $path, string $data)
 * @method static bool put(string $path, \Psr\Http\Message\StreamInterface|\Illuminate\Http\File|\Illuminate\Http\UploadedFile|string|resource $contents, mixed $options = [])
 * @method static bool setVisibility(string $path, string $visibility)
 * @method static bool writeStream(string $path, resource $resource, array $options = [])
 * @method static int lastModified(string $path)
 * @method static int size(string $path)
 * @method static resource|null readStream(string $path)
 * @method static string get(string $path)
 * @method static string getVisibility(string $path)
 * @method static string path(string $path)
 * @method static string temporaryUrl(string $path, \DateTimeInterface $expiration, array $options = [])
 * @method static string|false mimeType(string $path)
 * @method static string|false putFile(string $path, \Illuminate\Http\File|\Illuminate\Http\UploadedFile|string $file, mixed $options = [])
 * @method static string|false putFileAs(string $path, \Illuminate\Http\File|\Illuminate\Http\UploadedFile|string $file, string $name, mixed $options = [])
 *
 * @see \Illuminate\Filesystem\FilesystemManager
 */
class Storage {
    const TO_MB = 1048576;
    
    protected string $diskName = "local";
    protected ?Filesystem $disk = null;
    
    private static ?Storage $instance = null;
    
    public static function __callstatic(string $name, array $args) {
        $instance = static::make();
        return call_user_func_array([$instance->disk, $name], $args);
    }
    
    public static function url(string $file) {
        return Config::$pluginBaseUrl . '/storage/' . $file;
    }
    
    public static function getDimensions(string $path) {
        $fullPath = static::fullPath($path);
        return empty($fullPath) ? null : (new \Imagick($fullPath))->getImageGeometry();
    }
    
    public static function getAspectRatio(string $path) {
        $geo = static::getDimensions($path);
        return round($geo['width'] / $geo['height'], 2);
    }
    
    public static function matchesAspectRatio(string $path, float $targetRatio) {
        $aspect = static::getAspectRatio($path);
        return $targetRatio >= $aspect-0.1 && $targetRatio <= $aspect+0.1;
    }
    
    public static function isPng(string $path) {
        $mime = Storage::mimeType($path);
        return strtolower($mime) === 'image/png';
    }
    
    public static function sizeInMb(string $path) {
        $bytes = Storage::size($path);
        return round($bytes / static::TO_MB, 3);
    }
    
    public static function fullPath(string $relPath, bool $ensureExists = true) {
        $root = ConfigFile::get('filesystem', 'filesystems.disks.local.root');
        $path = $root . '/' . $relPath;
        return $ensureExists ? realpath($path) : $path;
    }
    
    private static function make() {
        if (static::$instance === null) {
            static::$instance = new static();
        }
        
        return static::$instance;
    }
    
    private function __construct() {
        $container = new Container();
        $container->instance('app', $container);
        $container['config'] = new Repository(ConfigFile::getConfigFile('filesystem'));
        
        $manager = new FilesystemManager($container);
        $this->disk = $manager->disk($this->diskName);
    }
}
