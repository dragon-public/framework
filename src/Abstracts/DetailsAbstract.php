<?php

namespace Dragon\Abstracts;

use Dragon\Url;
use Dragon\View;
use Illuminate\Database\Eloquent\Model;
use Dragon\Config;
use Dragon\User;
use Illuminate\Database\Eloquent\Collection;
use Dragon\Blade;

abstract class DetailsAbstract extends OptionsAbstract {
    protected string $listPageSlug = "";
    protected string $modelName = "";
    protected bool $readonly = false;
    protected bool $multiUser = false;
    protected bool $shouldSanitize = true;
    protected bool $shouldSave = true;
    protected string $multiUserUserIdColumn = 'user_id';
    protected string $pageTitle = "";
    protected string $pageDescription = "";
    protected string $pageBannerTemplate = "";
    
    protected array $goBackWithQuery = [
        // "query_key_1",
        // "query_key_2",
    ];
    protected array $sections = [];
    protected array $dbColumnToField = [];
    protected array $dbColumnToStaticValue = [];
    
    protected array $sanitizeTypes = [
        'text'		=> 'sanitize_text_field',
        'textarea'	=> 'sanitize_textarea_field',
        'url'		=> 'sanitize_url',
        'email'		=> 'sanitize_email',
    ];
    
    protected string $defaultSanitization = 'sanitize_text_field';
    
    public function render() {
        $this->setTokenfieldCallbacks();
        $this->setDbColumnToField();
        $this->setRequiredFields();
        $this->maybeGuessPageTitle();
        $this->saveSettings($this->pageData);
        
        $sections = ['[ALL]' => 'all'];
        if (!empty($this->sections)) {
            $sections = $this->sections;
        }
        
        $this->pageData['model'] = $this->maybeGetModel() ?? [];
        $this->pageData['fieldDescriptors'] = $this->dbColumnToField;
        $this->pageData['sections'] = $sections;
        $this->pageData['readonly'] = $this->readonly;
        
        if (!empty($this->listPageSlug)) {
            $this->pageData['goBack'] = Url::getAdminMenuLink($this->listPageSlug, $this->makeGoBackQuery());
        }
        $this->pageData['pageTitle'] = $this->pageTitle;
        $this->pageData['pageDescription'] = $this->pageDescription;
        $this->pageData['pageBanner'] = $this->pageBannerTemplate;
        
        echo Blade::view('table.details', $this->pageData);
    }
    
    public static function parseTokenfieldValue($value) {
        if (empty($value)) {
            return "[]";
        }
        
        $out = [];
        if ($value instanceOf Collection) {
            foreach ($value as $item) {
                $out[] = $item->id;
            }
        } elseif (is_string($value) && json_decode($value) !== false) {
            return $value;
        } elseif (is_array($value)) {
            $out = $value;
        }
        
        return json_encode($out);
    }
    
    protected function save() {
        $this->throwIfInvalid();
        $this->saveFields();
    }
    
    protected function throwIfInvalid() {
        //
    }
    
    protected function getId($key) {
        return $this->input->hasGet($key) && is_numeric($this->input->get($key)) ? (int)$this->input->get($key) : null;
    }
    
    protected function maybeGetModel() {
        if (is_numeric($this->input->get('id'))) {
            $model = $this->getModel((int)$this->input->get('id'));
            if ($this->multiUser) {
                return !empty($model) && $this->userCanAccessModel($model) ? $model : null;
            }
            
            return $model;
        }
        
        return $this->getModelForUser();
    }
    
    protected function getModelForUser() {
        return null;
    }
    
    protected function saveFields() {
        $model = $this->maybeGetModel();
        if (empty($model)) {
            $model = new $this->modelName();
        }
        
        foreach ($this->dbColumnToField as $dbColumn => $fieldData) {
            if (!empty($fieldData['fake_column']) && $fieldData['fake_column'] === true) {
                continue;
            }
            
            $fieldData['empty_is_null'] = !empty($fieldData['empty_is_null']);
            $data = $this->input->post('details_' . $dbColumn);
            if (empty($data) &&
                in_array('details_' . $dbColumn, $this->optionalFields) &&
                $fieldData['empty_is_null']
                ) {
                    $model->{$dbColumn}	= null;
                    continue;
                }
                
                $jsonArray = null;
                if ($fieldData['type'] === 'token' ||
                    ($fieldData['type'] === 'select' && !empty($fieldData['attributes']['multiple']))
                    ) {
                        $jsonArray = $data;
                        $data = json_encode($data);
                    }
                    
                    $data = $this->getSanitizedValue($fieldData, $data);
                    $this->throwIfEmpty($dbColumn, $data);
                    
                    if ($fieldData['type'] === 'checkbox' && empty($data)) {
                        $data = false;
                    }
                    
                    $model->{$dbColumn} = !is_null($jsonArray) ? $jsonArray : $data;
        }
        
        foreach ($this->dbColumnToStaticValue as $dbColumn => $value) {
            $model->{$dbColumn}	= $value;
        }
        
        if ($this->multiUser && $model->{$this->multiUserUserIdColumn} === null) {
            $model->{$this->multiUserUserIdColumn} = User::getUserId();
        }
        
        $this->handleAdditionalModelData($model);
        
        if ($this->shouldSave) {
            $model->save();
        }
        
        $this->finishingTouches($model);
    }
    
    protected function getSanitizedValue(array $fieldData, ?string $value) {
        if (!array_key_exists('sanitize', $fieldData)) {
            $fieldData['sanitize'] = true;
        }
        
        $shouldSanitize = $this->shouldSanitize && !empty($fieldData['sanitize']);
        if ($shouldSanitize) {
            $sanitizeFunc = $this->defaultSanitization;
            if (!empty($this->sanitizeTypes[$fieldData['type']])) {
                $sanitizeFunc = $this->sanitizeTypes[$fieldData['type']];
            }
            
            return $sanitizeFunc($value);
        } else {
            return $value;
        }
    }
    
    protected function getModel($id) {
        return call_user_func([$this->modelName, 'find'], $id);
    }
    
    protected function setDbColumnToField() {
        //
    }
    
    protected function handleAdditionalModelData(Model &$model) {
        //
    }
    
    protected function finishingTouches(Model $model) {
        //
    }
    
    protected function getMissingFields() {
        $mapped = [];
        foreach ($this->missingFields as $fieldName) {
            $dbColumn = substr($fieldName, strlen('details_'));
            $mapped[$fieldName] = $this->dbColumnToField[$dbColumn]['label'];
        }
        
        return $mapped;
    }
    
    private function makeGoBackQuery() {
        $out = [];
        foreach ($this->goBackWithQuery as $key) {
            $out[$key] = $this->input->get($key);
        }
        
        return $out;
    }
    
    private function setTokenfieldCallbacks() {
        foreach ($this->dbColumnToField as $column => $data) {
            if ($data['type'] === 'token') {
                $this->dbColumnToField[$column]['valueCallback'] = [static::class, 'parseTokenfieldValue'];
            }
        }
    }
    
    private function maybeGuessPageTitle() {
        if (empty($this->listPageSlug) || !empty($this->pageTitle)) {
            return;
        }
        
        $providers = Config::providers();
        $this->pageTitle = $providers['on_demand']['AdminMenu']::getTitleForSlug($this->listPageSlug);
    }
    
    private function setRequiredFields() {
        foreach ($this->dbColumnToField as $dbColumn => $fieldData) {
            if (!empty($fieldData['required'])) {
                $this->requiredFields[] = 'details_' . $dbColumn;
            } else {
                $this->optionalFields[] = 'details_' . $dbColumn;
            }
        }
    }
    
    private function userCanAccessModel(Model $model) {
        $modelUserId = $model->{$this->multiUserUserIdColumn};
        $userRole = User::getRole();
        $userId = User::getUserId();
        return $modelUserId === $userId || $userRole === 'administrator';
    }
    
    private function throwIfEmpty(string $dbColumn, string $data) {
        if (is_numeric($data) || !empty($data) || in_array('details_' . $dbColumn, $this->optionalFields)) {
            return;
        }
        
        $this->missingFields[] = 'details_' . $dbColumn;
        throw new OptionsMissingRequiredFieldsException();
    }
}
