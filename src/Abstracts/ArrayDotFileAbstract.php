<?php

namespace Dragon\Abstracts;

abstract class ArrayDotFileAbstract {
    public static function get($fileDot, $key, $default = null) {
        $array = static::getArrayByFileDot($fileDot);
        return arrayDot($array, $key, $default);
    }
    
    private static function getArrayByFileDot($fileDot) {
        $root = static::getFileBasePath();
        $pathData = explode('.', $fileDot);
        for ($i = 0; $i < count($pathData); $i++) {
            
            if ($i === count($pathData)-1) {
                
                $file = $root . $pathData[$i] . '.php';
                if (file_exists($file)) {
                    return require($file);
                } else {
                    return [];
                }
                
            } else {
                $root .= $pathData[$i] . '/';
            }
            
        }
    }
    
    abstract protected static function getFileBasePath();
}
