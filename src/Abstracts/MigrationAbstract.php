<?php

namespace Dragon\Abstracts;

abstract class MigrationAbstract {
    const TYPE_CREATE_TABLE	= 'CREATE_TABLE';
    const TYPE_ADD_COLUMN	= 'ADD_COLUMN';
    const TYPE_CHANGE_COLUMN = 'CHANGE_COLUMN';
    
    public $type = null;
    
    public function __construct() {
        $this->type = static::TYPE_CREATE_TABLE;
    }
}
