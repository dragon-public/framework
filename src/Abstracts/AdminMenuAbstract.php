<?php

namespace Dragon\Abstracts;

use Dragon\Config;
use Dragon\User;
use Dragon\Url;

abstract class AdminMenuAbstract {
    protected static $removeMenus = [
//         'tools.php' => [
//             'capabilities_to_see' => [
//                 'read',
//             ],
//             'redirect_to' => 'https://example.com', // Optional
//         ],
//         'edit.php?post_type=participants-database' => [
//             'capabilities_to_see' => [
//                 'manage_options',
//             ],
//             'redirect_to' => 'https://example.com',
//         ],
    ];
    
    protected static $menus = [
// 		'dragon-settings' => [
// 			'menu-text'		=> 'Main Menu Name',
// 			'page-title'	=> 'Page Title Text', // Optional
// 			'icon'			=> 'dashicons-admin-home', // Optional (use custom-icon for an image name.)
// 			'capabilities'	=> 'manage_options',
// 			'callback'		=> 'DragonApp\\Pages\\MyPage',
// 			'submenus' => [
// 				'dragon-slug' => [
// 					'menu-text'		=> 'Menu Text',
// 					'capabilities'	=> 'manage_options',
// 					'callback'		=> 'DragonApp\\Pages\\MyPage',
// 				],
// 			],
// 			'hidden' => [
// 				'dragon-slug2' => [
// 					'menu-text'		=> 'Hidden Menu',
// 					'capabilities'	=> 'manage_options',
// 					'callback'		=> 'DragonApp\\Pages\\MyHiddenPage',
// 				],
// 			],
// 		],
    ];
    
    public static function removeMenus() {
        $caps = User::getCapabilities();
        foreach (static::$removeMenus as $menuSlug => $data) {
            if (empty(array_diff($data['capabilities_to_see'], $caps))) {
                continue;
            }
            
            remove_menu_page($menuSlug);
            static::maybeRedirect($menuSlug, $data);
        }
    }
    
    public static function constructMenus() {
        foreach (static::$menus as $rootSlug => $rootData) {
            static::addMenuPage($rootSlug, $rootData);
            
            if (!empty($rootData['submenus'])) {
                static::addSubmenusForParent($rootSlug, $rootData, 'submenus');
            }
            
            if (!empty($rootData['hidden'])) {
                static::addSubmenusForParent($rootSlug, $rootData, 'hidden');
            }
        }
    }
    
    public static function getTitleForSlug(string $targetSlug) {
        foreach (static::$menus as $slug => $data) {
            if ($targetSlug === $slug) {
                return static::getTitleFromMenuData($data);
            }
            
            if (!empty($data['submenus'])) {
                $response = static::searchSubmenusForTitle($targetSlug, $data, 'submenus');
                if ($response !== false) {
                    return $response;
                }
            }
            
            if (!empty($data['hidden'])) {
                $response = static::searchSubmenusForTitle($targetSlug, $data, 'hidden');
                if ($response !== false) {
                    return $response;
                }
            }
        }
        
        return "";
    }
    
    protected static function getCountForMenuItem(string $menuItem) {
        return null;
    }
    
    private static function maybeRedirect(string $menuSlug, array $data) {
        $currentPage = $_SERVER['REQUEST_URI'];
        $currentPage = $currentPage === '/wp-admin/' ? $currentPage . 'index.php' : $currentPage;
        
        $targetPage = static::composeTargetPage($menuSlug);
        if ($targetPage !== $currentPage) {
            return;
        }
        
        if (empty($data['redirect_to'])) {
            die('Access Denied.');
        }
        
        if (strpos($data['redirect_to'], 'http') !== 0) {
            $data['redirect_to'] = Url::getAdminMenuLink($data['redirect_to']);
        }
        
        wp_redirect($data['redirect_to']);
        die();
    }
    
    private static function composeTargetPage(string $menuSlug) {
        $targetPage = null;
        if (strpos($menuSlug, '.php') !== false) {
            $targetPage = '/wp-admin/' . $menuSlug;
        } else {
            $targetPage = '/wp-admin/admin.php?page=' . $menuSlug;
        }
        
        return $targetPage;
    }
    
    private static function addSubmenusForParent(string $rootSlug, array $rootData, string $type) {
        foreach ($rootData[$type] as $slug => $subData) {
            if ($type === 'hidden') {
                $subData['hidden'] = true;
            }
            static::addSubmenuPage($rootSlug, $slug, $rootData, $subData);
        }
    }
    
    private static function searchSubmenusForTitle(string $targetSlug, array $data, string $type) {
        foreach ($data[$type] as $submenuSlug => $data) {
            if ($targetSlug === $submenuSlug) {
                return static::getTitleFromMenuData($data);
            }
        }
        
        return false;
    }
    
    private static function getTitleFromMenuData(array $data) : string {
        if (!empty($data['page-title'])) {
            return $data['page-title'];
        }
        
        if (!empty($data['menu-text'])) {
            return $data['menu-text'];
        }
        
        return "";
    }
    
    private static function addMenuPage(string $slug, array $data) {
        if (!empty($data['custom-icon'])) {
            $data['icon'] = Config::$pluginBaseUrl . 'resources/img/' . $data['custom-icon'];
        }
        
        $data['callback'] = function () use ($data) {
            if (!user_can(User::getUserId(), $data['capabilities'])) {
                return false;
            }
        };
        
        $data['icon']		= empty($data['icon']) ? 'dashicons-admin-home' : $data['icon'];
        $data['page-title']	= empty($data['page-title']) ? $data['menu-text'] : $data['page-title'];
        add_menu_page(
            __($data['page-title'], Config::$namespace),
            static::maybeAddMenuCount($slug, __($data['menu-text'], Config::$namespace)),
            $data['capabilities'],
            $slug,
            $data['callback'],
            $data['icon']
            );
    }
    
    private static function maybeAddMenuCount(string $slug, string $menuText) {
        $count = static::getCountForMenuItem($slug);
        if (!empty($count)) {
            $menuText .= ' <span class="awaiting-mod">' . $count . '</span>';
        }
        
        return $menuText;
    }
    
    private static function addSubmenuPage(string $parentSlug, string $slug, array $parentData, array $data) {
        $parentSlug = !empty($data['hidden']) ? "" : $parentSlug;
        
        $data['callback'] = empty($data['callback']) ? $parentData['callback'] : $data['callback'];
        $data['page-title']	= empty($data['page-title']) ? $data['menu-text'] : $data['page-title'];
        $data['capabilities'] = empty($data['capabilities']) ? $parentData['capabilities'] : $data['capabilities'];
        
        $data['callback'] = function () use ($data) {
            if (!user_can(User::getUserId(), $data['capabilities'])) {
                return false;
            }
            
            $callback = new $data['callback']();
            $callback->render();
        };
        
        add_submenu_page(
            $parentSlug,
            $data['page-title'],
            static::maybeAddMenuCount($slug, __($data['menu-text'], Config::$namespace)),
            $data['capabilities'],
            $slug,
            $data['callback']
            );
    }
}
