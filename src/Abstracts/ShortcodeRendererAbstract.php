<?php

namespace Dragon\Abstracts;

abstract class ShortcodeRendererAbstract extends PageAbstract {
	protected array $attributes = [];
	protected array $pageData = [];
	protected ?string $content = null;
	
	public function __construct(array $attributes = []) {
		parent::__construct();
		if (empty($attributes)) {
			return;
		}
		
		$this->attributes	= $attributes['attributes'];
		$this->pageData		= $attributes['pageData'];
		$this->content		= $attributes['shortcodeContent'];
	}
	
	abstract public function render();
}
