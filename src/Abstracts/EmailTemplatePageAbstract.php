<?php

namespace Dragon\Abstracts;

use Illuminate\Database\Eloquent\Model;

abstract class EmailTemplatePageAbstract extends DetailsAbstract {
    protected function handleAdditionalModelData(Model &$model) {
        $posted = $this->input->post();
        $json = [];
        foreach ($posted as $key => $val) {
            if (strtoupper($key) === $key) {
                $json[$key] = $val;
            }
        }
        
        $model->message = json_encode($json);
    }
}
