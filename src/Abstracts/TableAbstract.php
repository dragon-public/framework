<?php

namespace Dragon\Abstracts;

use Dragon\Url;
use Dragon\View;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Dragon\Csv;
use Illuminate\Support\Str;
use Dragon\User;
use Dragon\Paging;
use Illuminate\Database\Eloquent\Collection;
use Dragon\Blade;

abstract class TableAbstract extends PageAbstract {
    protected $headers = [
        'Title',
        'Comments',
        'Author',
    ];
    
    protected $viewPageSlug = "";
    protected $createPageSlug = "";
    protected $updatePageSlug = "";
    protected $currentPageSlug = "";
    
    protected $viewLink = "";
    protected $createLink = "";
    protected $updateLink = "";
    protected $currentPageLink = "";
    
    protected string $pageBannerTemplate = "";
    
    protected $customLinks = [
// 		'slug-name'	=> [
// 			'method'	=> 'getGeneratedLink',
// 			'name'		=> 'Display Name',
// 		],
    ];
    
    protected bool $multiLineLinks = false;
    
    protected $actionButtons = [
        //
    ];
    
    protected $bulkActions = [
// 		'mark_read' => [
// 			'label' => 'Select One',
// 			'options' => [
// 				'read' => 'Mark All as Read',
// 			],
// 		],
    ];
    
    protected $pageText = "";
    
    protected $canView = false;
    protected $canCreate = false;
    protected $canUpdate = false;
    protected $canDelete = false;
    
    protected $searchable = false;
    
    protected $searchableByDate = false;
    protected $dateSearchColumnName = Model::CREATED_AT;
    
    protected bool $multiUser = false;
    protected string $multiUserUserIdColumn = 'user_id';
    
    protected $filters = [
// 		'my_filter'	=> [
// 			'label'	=> 'My Filter',
// 			'options'	=> [
// 				'option1'	=> 'Option 1',
// 			],
// 		],
    ];
    
    protected $rows = [
// 		[
// 			[
// 				'data'	=> 'Testing',
// 				'links'	=> [
// 					'view'	=> [
// 						'link'	=> '#',
// 					],
// 					'edit'	=> [
// 						'link'	=> '#',
// 						'class'	=> 'my-edit',
// 					],
// 					'delete'	=> [
// 						'link'	=> '#',
// 					],
// 				],
// 			],
// 			[
// 				'data'	=> 'Testing 1',
// 			],
// 			[
// 				'data'	=> 'Testing 2',
// 			],
// 		],
    ];
    
    protected ?Paging $paging = null;
    protected int $pagingPerPage = 20;
    protected string $paginationQueryVar = 'paged';
    
    protected bool $isSortable = false;
    
    protected $csvHeaders = [];
    protected string $modelName = "";
    
    protected array $detailsWithQuery = [
//         "query_key_1",
//         "query_key_2",
    ];
    
    public function render() {
        $this->maybeHandleBulkActions();
        $this->paging = new Paging($this->pagingPerPage, $this->paginationQueryVar);
        
        if (empty($this->createPageSlug)) {
            $this->createPageSlug = $this->currentPageSlug . '-details';
        }
        
        if (empty($this->updatePageSlug)) {
            $this->updatePageSlug = $this->currentPageSlug . '-details';
        }
        
        $this->createLink = $this->canCreate ? Url::getAdminMenuLink($this->createPageSlug, $this->makeDetailsQuery()) : '';
        $this->updateLink = $this->canUpdate ? Url::getAdminMenuLink($this->updatePageSlug, $this->makeDetailsQuery()) : '';
        $this->viewLink = $this->canView ? Url::getAdminMenuLink($this->viewPageSlug, $this->makeDetailsQuery()) : '';
        $this->currentPageLink = Url::getAdminMenuLink($this->currentPageSlug, $this->makeDetailsQuery());
        $this->maybeDeleteListItem();
        $this->display();
        
        $this->pageData = array_merge([
            'createLink'	=> $this->createLink,
            'pageText'		=> $this->pageText,
            'headers'		=> $this->headers,
            'rows'			=> $this->rows,
            'totalRows'     => count($this->rows),
            'paging'		=> $this->paging,
            'pagingData'    => (empty($this->paging) ? [] : $this->paging->getPaging()),
            'filters'		=> $this->filters,
            'searchable'	=> $this->searchable,
            'dateSeachable'	=> $this->searchableByDate,
            'actionButtons'	=> $this->actionButtons,
            'bulkActions'	=> $this->bulkActions,
            'isSortable'	=> $this->isSortable,
            'multiLineLinks' => $this->multiLineLinks,
            'pageBanner'    => $this->pageBannerTemplate,
            'input'         => $this->input,
        ], $this->pageData);
        
        echo Blade::view('table.admin-table', $this->pageData);
    }
    
    protected function maybeHandleBulkActions() {
        foreach (array_keys($this->bulkActions) as $postKey) {
            $actionIds = $this->input->post('action_ids');
            $posted = $this->input->post($postKey);
            if (!empty($posted) && !empty($actionIds)) {
                $method = 'handleBulkAction' . str_replace(' ', '', camelTitle($postKey));
                $this->{$method}($posted, explode(',', $actionIds));
            }
        }
    }
    
    protected function display() {
        $this->populateRows();
    }
    
    protected function populateRows() {
        $search = $this->input->get('s');
        $list = $this->getModelQuery();
        if (empty($list)) {
            return;
        }
        
        $role = User::getRole();
        if ($this->multiUser && $role !== 'administrator') {
            $list = $list->where($this->multiUserUserIdColumn, User::getUserId());
        }
        
        if ($this->searchableByDate) {
            $list = $this->maybeAddDateSearch($list);
        }
        
        $this->paging->addPaging($list);
        
        if (!empty($search)) {
            $list = $list->appends(['s' => $search]);
        }
        
        foreach ($list as $listItem) {
            
            if ($this->addRow($listItem) === false) {
                continue;
            }
            
            $idKey = $listItem->getKeyName();
            $this->maybeAddRowLinks($listItem->{$idKey}, $listItem);
            
        }
    }
    
    protected function getModelQuery() {
        //
    }
    
    protected function addRow($listItem) {
        //
    }
    
    protected function maybeDeleteListItem() {
        if ($this->canDelete && is_numeric($this->input->get('delete'))) {
            
            $model = $this->getModelToDelete((int)$this->input->get('delete'));
            
            if (!empty($model)) {
                $model->delete();
                Url::redirect($this->currentPageLink, 'Deleted.');
            }
            
            Url::redirect($this->currentPageLink, 'Could not delete this item.');
        }
    }
    
    protected function getModelToDelete($id) {
        if (empty($this->modelName)) {
            return;
        }
        
        $model = $this->modelName::find($id);
        if (empty($model)) {
            return;
        }
        
        if ($this->multiUser && User::getRole() !== 'administrator' &&
            $model->{$this->multiUserUserIdColumn} !== User::getUserId()
            ) {
                return;
            }
            
            return $model;
    }
    
    protected function maybeAddRowLinks($modelId, $model) {
        $rowId = count($this->rows)-1;
        $this->rows[$rowId][0]['links'] = [];
        
        if (empty($this->rows[$rowId][0]['id'])) {
            $this->rows[$rowId][0]['id'] = $modelId;
        }
        
        if ($this->canView && $this->canViewRow($model)) {
            
            $this->rows[$rowId][0]['links']['view'] = [
                'link'	=> $this->getViewLink($modelId),
            ];
            
        }
        
        if ($this->canUpdate && $this->canUpdateRow($model)) {
            
            $this->rows[$rowId][0]['links']['edit'] = [
                'link'	=> $this->getUpdateLink($modelId),
            ];
            
        }
        
        if ($this->canDelete && $this->canDeleteRow($model)) {
            
            $this->rows[$rowId][0]['links']['delete'] = [
                'link'	=> $this->getDeleteLink($modelId),
            ];
            
        }
        
        foreach ($this->customLinks as $type => $data) {
            
            if ($this->{'rowHas' . kababToCamel($type)}($model)) {
                
                $this->rows[$rowId][0]['links'][$type] = [
                    'link'	=> $this->{$data['method']}($model),
                    'name'	=> $data['name'],
                    ];
                
            }
            
        }
    }
    
    protected function canViewRow(Model $model) {
        return $this->canView;
    }
    
    protected function canUpdateRow(Model $model) {
        return $this->canUpdate;
    }
    
    protected function canDeleteRow(Model $model) {
        return $this->canDelete;
    }
    
    protected function getDeleteLink($id) {
        return $this->currentPageLink .
        '&delete=' . $id .
        '&paged=' . $this->paging->getPaging()['current_page'] .
        '&s=' . $this->input->get('s');
    }
    
    protected function getUpdateLink($id) {
        return $this->updateLink . '&id=' . $id;
    }
    
    protected function getViewLink($id) {
        return $this->viewLink . '&id=' . $id;
    }
    
    protected function maybeAddDateSearch(Builder $query) {
        $startDate = $this->input->get('date-start');
        $endDate = $this->input->get('date-end');
        
        if (!empty($startDate)) {
            $query = $query->whereDate($this->dateSearchColumnName, '>=', $startDate);
        }
        
        if (!empty($endDate)) {
            $query = $query->whereDate($this->dateSearchColumnName, '<=', $endDate);
        }
        
        return $query;
    }
    
    protected function maybeDownloadToCsv(string $queryParam, string $filename) {
        $shouldDownload = $this->input->get($queryParam) !== null;
        
        if ($shouldDownload === false) {
            return;
        }
        
        $query = $this->getModelQuery();
        
        $role = User::getRole();
        if ($this->multiUser && $role !== 'administrator') {
            $query = $query->where($this->multiUserUserIdColumn, User::getUserId());
        }
        
        $data = $query->get();
        if (empty($data)) {
            return;
        }
        
        $this->adjustCsvHeaders($data);
        
        $csv = new Csv();
        $csv->headers = $this->csvHeaders;
        
        foreach ($data as $row) {
            $rowData = $this->{Str::camel($queryParam) . 'AddRow'}($row);
            if (!empty($rowData)) {
                $csv->rows[] = $rowData;
            }
        }
        
        $csv->download($filename);
    }
    
    protected function adjustCsvHeaders(Collection $data) {
        //
    }
    
    private function makeDetailsQuery() {
        $out = [];
        foreach ($this->detailsWithQuery as $key) {
            $out[$key] = $this->input->get($key);
        }
        
        return $out;
    }
}
