<?php

namespace Dragon\Abstracts;

use Dragon\Config;
use Dragon\ConfigFileNotFoundException;
use Dragon\Email\Email;
use Dragon\Traits\HasHooks;
use Dragon\PseudoSession;

abstract class PluginHooksAbstract {
	use HasHooks;
	
	protected static $dragonActions = [
		'wp_mail_failed' => [
			[Email::class, 'logFailedMailings'],
		],
		'wp_logout' => [
			[PseudoSession::class, 'destroy'],
		],
		'wp_login' => [
			[PseudoSession::class, 'destroy'],
		],
	];
	
	protected static $dragonFilters = [
		'upload_mimes'	=> [
			[
				'callback' => [], // Added below
				'args' => 2,
			],
		],
	];
	
	public static function init() {
		$providers = Config::providers();
		$role = $providers['global']['Role'];
		static::$dragonFilters['upload_mimes'][0]['callback'] = [$role, 'filterUploadableMimeTypes'];
		
		static::$actions = array_merge(static::$dragonActions, static::$actions);
		static::$filters = array_merge(static::$dragonFilters, static::$filters);
		
		PseudoSession::start();
		static::setActions();
		static::setFilters();
	}
	
	public static function shutdown() {
		try {
			Config::save();
		} catch (ConfigFileNotFoundException $e) {
			// Allow WP to shut down without issues.
		}
	}
}
