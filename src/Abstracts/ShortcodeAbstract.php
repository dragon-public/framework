<?php

namespace Dragon\Abstracts;

abstract class ShortcodeAbstract {
	protected static bool $shouldFixAttributes = false;
	protected static $shortcodes = [
		// 		'shortcode_name' => [
		// 			'PageName' => [
		// 				'default'	=> true,
		// 				'callback'	=> PageName::class,
		// 			],
		// 		],
	];
	
	public static function init() {
		if (wp_is_json_request()) {
			return;
		}
		
		foreach (static::$shortcodes as $tag => $options) {
			add_shortcode($tag, function ($attributes, $content) use ($tag, $options) {
				if (empty($attributes)) {
					$attributes = [];
				}
				
				$attributes['attributes'] = static::parseAttributes($attributes);
				$attributes['pageData'] = $options;
				$attributes['shortcodeContent'] = $content;
				
				return call_user_func([static::class, 'handleShortcode'], $attributes);
			});
		}
	}
	
	public static function handleShortcode(array $attributes) {
		$attributes['page'] = static::getPageClass($attributes);
		return static::createPage($attributes['page'], $attributes);
	}
	
	public static function createPage($className, array $attributes) {
		if (class_exists($className)) {
			$componentPage = new $className($attributes);
			return $componentPage->render();
		}
	}
	
	private static function parseAttributes(array $attributes) {
		if (array_key_exists(0, $attributes) === false) {
			return $attributes;
		}
		
		if (static::$shouldFixAttributes) {
			$attributes = static::fixAttributesArraySpaces($attributes);
		}
		
		$out = [];
		foreach ($attributes as $attribute) {
			$parts = explode("=", $attribute);
			$key = trim($parts[0]);
			array_shift($parts);
			$value = implode("=", $parts);
			if (substr($value, 0, 1) === '"' || substr($value, 0, 1) === "'") {
				$value = substr($value, 1);
			}
			
			if (substr($value, -1, 1) === '"' || substr($value, -1, 1) === "'") {
				$value = substr($value, 0, strlen($value)-1);
			}
			
			$out[$key] = $value;
		}
		
		return $out;
	}
	
	private static function fixAttributesArraySpaces(array $attributes) {
		$out = [];
		for ($i = 0; $i < count($attributes) ; $i++) {
			$element = $attributes[$i];
			$lastChar = substr($element, -1, 1);
			if ($lastChar !== '"') {
				$out[] = $element . " " . $attributes[$i+1];
				$i++;
			} else {
				$out[] = $element;
			}
		}
		
		return $out;
	}
	
	private static function getPageClass(array $attributes) {
		if (empty($attributes['page'])) {
			foreach ($attributes['pageData'] as $pageName => $options) {
				if (!empty($options['default']) && $options['default'] === true) {
					return $options['callback'];
				}
			}
		} else {
			$callbackData = $attributes['pageData'][$attributes['page']];
			return is_array($callbackData) ? $callbackData['callback'] : $callbackData;
		}
	}
}
