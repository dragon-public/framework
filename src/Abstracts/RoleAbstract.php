<?php

namespace Dragon\Abstracts;

use Dragon\User;

abstract class RoleAbstract {
    protected static array $roles = [
//         'my-role' => [
//             'name' => 'My Role',
//             'capabilities' => [
//                 'read',
//                 'capability1',
//                 'capability2',
//             ],
//         ],
    ];
    
    protected static array $uploadableMimes = [
//         'my-role' => [
//             "jpg|jpeg|jpe"	=> "image/jpeg",
//             "png"			=> "image/png",
//             "webp"			=> "image/webp",
//         ],
    ];
    
    protected static bool $restrictMediaToUser = true;
    protected static bool $addAllToAdmin = true;
    
    public static function init() {
        foreach (static::$roles as $role => $roleData) {
            $capabilities = array_merge(['read'], $roleData['capabilities']);
            $capabilitiesEnabled = array_fill(0, count($capabilities), true);
            remove_role($role);
            add_role(
                $role,
                $roleData['name'],
                array_combine($capabilities, $capabilitiesEnabled)
                );
            
            array_shift($capabilities);
            $admin = get_role(User::ROLE_ADMIN);
            foreach ($capabilities as $capability) {
                if (static::$addAllToAdmin) {
                    $admin->add_cap($capability);
                } else {
                    $admin->remove_cap($capability);
                }
            }
        }
    }
    
    public static function restrictMediaAccess($query) {
        if (!static::$restrictMediaToUser) {
            return $query;
        }
        
        $userId = User::getUserId();
        if (!empty($userId) && !current_user_can('activate_plugins') && !current_user_can('edit_others_posts') ) {
            $query['author'] = $userId;
        }
        return $query;
    }
    
    public static function filterUploadableMimeTypes(array $types, $user) {
        $userId = null;
        if (is_int($user)) {
            $userId = $user;
        } elseif ($user InstanceOf \WP_User) {
            $userId = $user->ID;
        }
        
        $role = User::getRole($userId);
        $role = $role === false ? 'subscriber' : $role;
        if (array_key_exists($role, static::$uploadableMimes)) {
            return static::$uploadableMimes[$role];
        }
        
        return $types;
    }
    
    public static function getRoleName(?string $role) {
        $roles = static::getRoles();
        return empty($roles[$role]) ? ucfirst($role) : $roles[$role];
    }
    
    public static function getRoles() {
        $roles = wp_roles();
        $out = [];
        foreach ($roles->roles as $role => $data) {
            $out[$role] = $data['name'];
        }
        
        return $out;
    }
}
