<?php

namespace Dragon\Abstracts;

use Dragon\Traits\ValidatesForms;

abstract class AdminFormAbstract extends PageAbstract {
    use ValidatesForms;
}
