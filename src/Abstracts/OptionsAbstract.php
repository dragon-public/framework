<?php

namespace Dragon\Abstracts;

use Dragon\Config;
use Dragon\View;
use Dragon\Input;
use Dragon\User;

class OptionsInvalidNonceException extends \Exception{}
class OptionsMissingRequiredFieldsException extends \Exception{}

abstract class OptionsAbstract extends PageAbstract {
    protected $userId = null;
    protected $saveButton = null;
    protected $adminForm = true;
    protected $requiredFields = [];
    protected $optionalFields = [];
    protected $encryptedFields = [];
    protected $successMessage = 'All settings saved successfully.';
    protected $badNonceMessage = 'An invalid nonce was supplied for the form. Try refreshing the page and reentering your information.';
    protected $missingRequiredMessage = 'One or more required fields are missing. Please enter the missing information and try again.';
    protected $genericError = 'An error occurred.';
    protected bool $hasError = false;
    protected $settingsAreGlobal = true;
    protected $missingFields = [];
    protected bool $shouldSanitize = true;
    
    protected function saveSettings(&$pageData) {
        $this->input = new Input();
        
        if (is_null($this->saveButton)) {
            $this->saveButton = Config::$namespace . '_save';
        }
        
        if (!empty($this->input->post($this->saveButton))) {
            try {
                
                $this->handleSaveSettings();
                if (!$this->hasError) {
                    $this->makeSuccessNotice($this->successMessage);
                }
                
            } catch (OptionsInvalidNonceException $e) {
                $this->makeErrorNotice($this->badNonceMessage);
            } catch (OptionsMissingRequiredFieldsException $e) {
                $this->setMissingRequiredFieldsNotice();
            } catch (\Exception $e) {
                $this->makeErrorNotice($e->getMessage());
            }
        }
    }
    
    protected function updateOptionIfSet($option) {
        if ($this->input->hasPost($option)) {
            $this->updateOption($option);
        }
    }
    
    protected function updateOption($option) {
        $value = $this->input->post($option);
        $value = $this->prepareOptionValue($option, $value);
        if (in_array($option, $this->encryptedFields)) {
            $value = dragonEncrypt($value);
        }
        
        $saveValue = $this->shouldSanitize ? sanitize_textarea_field($value) : $value;
        if ($this->settingsAreGlobal) {
            update_option($option, $saveValue);
        } else {
            User::setMeta($option, $saveValue, $this->userId);
        }
    }
    
    protected function prepareOptionValue($option, $value) {
        return $value;
    }
    
    protected function save() {
        foreach($this->requiredFields as $field) {
            $this->updateOption($field);
        }
        
        foreach($this->optionalFields as $field) {
            $this->updateOptionIfSet($field);
        }
    }
    
    protected function getMissingFields() {
        return $this->missingFields;
    }
    
    protected function setMissingRequiredFieldsNotice() {
        $missingList = implode(', ', $this->getMissingFields());
        $this->makeErrorNotice($this->missingRequiredMessage . '<br />Missing: ' . $missingList);
    }
    
    private function handleSaveSettings() {
        $this->throwIfMissingRequiredFields();
        $this->throwIfBadNonce(Config::$namespace . "_nonce");
        
        $this->save();
    }
    
    private function throwIfMissingRequiredFields() {
        if ($this->areAllRequiredFieldsPresent() === false) {
            throw new OptionsMissingRequiredFieldsException();
        }
    }
    
    private function areAllRequiredFieldsPresent() {
        $this->missingFields = [];
        foreach ($this->requiredFields as $field) {
            if (($this->input->hasPost($field) === false || $this->input->post($field) === '') &&
                empty($_FILES[$field]['name'])
                ) {
                    $this->missingFields[] = $field;
                }
        }
        
        return empty($this->missingFields);
    }
    
    private function throwIfBadNonce($pageNamespace) {
        if ($this->adminForm === true && $this->isValidNonce($pageNamespace) === false) {
            throw new OptionsInvalidNonceException();
        }
    }
    
    private function isValidNonce($pageNamespace) {
        return wp_verify_nonce($this->input->post($pageNamespace), $pageNamespace);
    }
}
