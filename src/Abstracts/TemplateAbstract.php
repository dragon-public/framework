<?php

namespace Dragon\Abstracts;

use Dragon\Config;

abstract class TemplateAbstract {
	protected ?string $templateName = null;
	protected ?string $templateHtmlContent = null;
	protected string $type = '';
	protected array $templateData = [];
	
	private ?string $compiledMessage = null;
	
	public function __construct(array $templateData = [], ?string $templateHtml = null) {
		$this->templateData = $templateData;
		$this->templateHtmlContent = $templateHtml;
		$this->compile();
	}
	
	public function getCompiledMessage() {
		return $this->compiledMessage;
	}
	
	protected function compile() {
		$this->compiledMessage = $this->makeTemplateFor($this->templateData, $this->templateName);
	}
	
	protected static function replaceVariables(array $messageVars, string $text) : string {
		$tokens = array_keys($messageVars);
		array_walk($tokens, function (&$value, $key) {
			$value = '##' . $value . '##';
		});
			
			return str_replace($tokens, array_values($messageVars), (string)$text);
	}
	
	private function makeTemplateFor(array $data = [], ?string $templateName = null) : ?string {
		$rawTemplate = $this->templateHtmlContent;
		if (!empty($templateName)) {
			$filename = Config::$pluginDir . '/resources/views/' . $this->type . '/' . $templateName . '.html';
			if (!file_exists($filename)) {
				return null;
			}
			
			$rawTemplate = file_get_contents($filename);
		}
		
		return static::replaceVariables($data, $rawTemplate);
	}
}
