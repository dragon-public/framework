<?php

namespace Dragon\Abstracts;

abstract class EmailTemplateAbstract extends TemplateAbstract {
	protected ?string $templateName = null;
	protected ?string $templateHtmlContent = null;
	protected string $type = 'emails';
}
