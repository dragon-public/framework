<?php

namespace Dragon\Abstracts;

use Dragon\Config;
use Dragon\Input;

abstract class PageAbstract {
    protected array $pageData = [];
    protected ?Input $input = null;
    
    public function __construct() {
        $this->input = new Input();
    }
    
    protected function makeErrorNotice($message) {
        $this->makeNotice('error', $message);
    }
    
    protected function makeSuccessNotice($message) {
        $this->makeNotice('success', $message);
    }
    
    protected function makeNotice($type, $message) {
        wp_admin_notice($message, ['type' => $type]);
    }
    
    protected function makeNonce() {
        return wp_create_nonce(Config::$namespace . '_nonce');
    }
    abstract public function render();
}
