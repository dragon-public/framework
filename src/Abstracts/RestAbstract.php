<?php

namespace Dragon\Abstracts;

use Dragon\Config;

abstract class RestAbstract {
    protected static $routes = [
//         '{ns}/{version}' => [
//         	'endpoint-name' => [
//         		'methods' => \WP_REST_Server::READABLE,
//         		'callback' => [RestAbstract::class, 'handleEndpointName'],
//         		'permission_callback' => [RestAbstract::class, 'handlePermissionCallback'],
//         	],
//         ],
    ];
    
    public static function init() {
        add_action('rest_api_init', [static::class, 'registerRoutes']);
    }
    
    public static function registerRoutes() {
        foreach (static::$routes as $namespace => $endpointData) {
            $namespace = str_replace('{ns}', Config::$namespace, (string)$namespace);
            foreach ($endpointData as $endpointName => $endpointConfig) {
                if (empty($endpointConfig['permission_callback'])) {
                    $endpointConfig['permission_callback'] = '__return_true';
                }
                register_rest_route($namespace, $endpointName, $endpointConfig);
            }
        }
    }
}
