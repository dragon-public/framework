<?php

namespace Dragon\Abstracts;

use Dragon\Input;

abstract class AdminInitAbstract {
	// Set this to any download query params so headers don't get sent before it.
	protected static array $downloadQueries = [
		//'export_csv',
	];
	
	public static function handleDownloads() {
		$input = new Input();
		$isDownload = $input->hasGet(static::$downloadQueries);
		if (!$isDownload) {
			return;
		}
		
		static::download();
	}
	
	abstract protected static function download();
}
