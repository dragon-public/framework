<?php

namespace Dragon\Abstracts;

use Dragon\Config;
use Dragon\Input;
use Dragon\Captcha;
use Dragon\Traits\ValidatesForms;
use Dragon\Blade;

class FrontEndFormBadCaptchaException extends \Exception{}

abstract class FrontEndFormAbstract extends ShortcodeRendererAbstract {
	use ValidatesForms;
	
	protected bool $hasCaptcha = false;
	
	protected function extraSubmissionValidation() {
		try {
			$this->throwIfBadCaptcha();
		} catch (FrontEndFormBadCaptchaException $e) {
			
			$this->handleBadCaptcha();
			return false;
			
		}
		
		return true;
	}
	
	private function throwIfBadCaptcha() {
		if ($this->hasCaptcha === false) {
			return;
		}
		
		$captcha = new Captcha();
		if ($captcha->isValid() === false) {
			throw new FrontEndFormBadCaptchaException();
		}
	}
	
	protected function handleBadCaptcha() {
		$this->makeErrorNotice($this->badCaptchaMessage);
	}
	
	protected function makeNotice($type, $message) {
		$this->pageData['notice'] = Blade::view('notice', [
			'type' => $type,
			'message' => $message,
		]);
	}
}
