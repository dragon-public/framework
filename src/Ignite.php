<?php

namespace Dragon;

class Ignite {
	public static function fire() {
		setlocale(LC_MONETARY, 'en_US.UTF-8');
		Config::open();
		
		FileSystem::includeClasses();
		
		DB::make();
	}
}
