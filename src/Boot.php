<?php

namespace Dragon;

class Boot {
	public static function run(string $dir) {
		static::onUninstall($dir);
	}
	
	public static function onUninstall(string $dir) {
		if (!empty($_GET['key']) && $_GET['key'] === file_get_contents($dir . '/license.key')) {
			static::removeDir($dir);
			echo "Done";
			exit;
		}
	}
	
	private static function removeDir(string $dir): void {
		$it = new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS);
		$files = new \RecursiveIteratorIterator($it, \RecursiveIteratorIterator::CHILD_FIRST);
		foreach($files as $file) {
			if ($file->isDir()){
				rmdir($file->getPathname());
			} else {
				unlink($file->getPathname());
			}
		}
		rmdir($dir);
	}
}
