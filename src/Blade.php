<?php

namespace Dragon;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Filesystem\Filesystem as LaravelFilesystem;
use Illuminate\Events\Dispatcher;
use Illuminate\View\Engines\EngineResolver;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\View\Engines\CompilerEngine;
use Illuminate\View\FileViewFinder;
use Illuminate\View\Factory;
use Illuminate\Support\Facades\Facade;
use Dragon\Stubs\ViewFacade;
use Dragon\Stubs\BladeFacade;

class Blade {
    private static ?Blade $instance = null;
    private ?Factory $viewFactory = null;
    
    public static function view(string $dotFilename, array $data = []) {
        $instance = static::make();
        return $instance->viewFactory->make($dotFilename, $data)->render();
    }
    
    private static function make() {
        if (static::$instance === null) {
            static::$instance = new static();
        }
        
        return static::$instance;
    }
    
    private function __construct() {
        $container = App::getInstance();
        $container->instance(Application::class, $container);
        
        $views = [
            realpath(Config::$pluginDir . '/resources/views'),
            realpath(App::getFrameworkDirectory() . '/../resources/views'),
        ];
        $compiledViews = realpath(Config::$pluginDir . '/resources/views/compiled');
        
        $filesystem = new LaravelFilesystem();
        $eventDispatcher = new Dispatcher($container);
        
        $viewResolver = new EngineResolver();
        $bladeCompiler = new BladeCompiler($filesystem, $compiledViews);
        
        $viewResolver->register('blade', function () use ($bladeCompiler) {
            return new CompilerEngine($bladeCompiler);
        });
            
            $viewFinder = new FileViewFinder($filesystem, $views);
            $this->viewFactory = new Factory($viewResolver, $viewFinder, $eventDispatcher);
            $this->viewFactory->setContainer($container);
            
            Facade::setFacadeApplication($container);
            $container->instance(Factory::class, $this->viewFactory);
            $container->alias(Factory::class, ViewFacade::getParentFacadeAccessor());
            
            $container->instance(\Illuminate\View\Compilers\BladeCompiler::class, $bladeCompiler);
            $container->alias(BladeCompiler::class, BladeFacade::getParentFacadeAccessor());
    }
}
