<?php

namespace Dragon;

class FileSystem {
    const IN_HEADER = false;
    
    protected static $pluginClassNameToFile = [
        // static::class => 'path/from/plugin/root/to/its/class.php',
    ];
    
    protected static $classToFile = [
        //WP_List_Table::class => 'wp-admin/includes/class-wp-list-table.php',
    ];
    
    protected static $functionToFile = [
        'wp_delete_user' => 'wp-admin/includes/user.php',
    ];
    
    private static bool $alreadySetAjaxUrl = false;
    
    public static function includeFunctions() {
        foreach (static::$functionToFile as $functionName => $relativePath) {
            
            $filePath = ABSPATH . $relativePath;
            return static::includeFunction($functionName, $filePath);
            
        }
    }
    
    public static function includeClasses() {
        foreach (static::$classToFile as $className => $relativePath) {
            
            $filePath = ABSPATH . $relativePath;
            return static::includeClassFile($className, $filePath);
            
        }
    }
    
    public static function includeClass($className, $pluginName) {
        $relativePath = static::$pluginClassNameToFile[$className];
        $filePath = static::getPluginPath($pluginName) . $relativePath;
        return static::includeClassFile($className, $filePath);
    }
    
    public static function isPluginActive($pluginPath) {
        if (!function_exists('is_plugin_active')) {
            require_once(ABSPATH . 'wp-admin/includes/plugin.php');
        }
        
        return is_plugin_active($pluginPath);
    }
    
    public static function getCurrentPageSlug() {
        $homeURL = home_url('/');
        $currentURL = (string) @get_page_link();
        $currentPath = substr($currentURL, strlen($homeURL));
        $currentPathWithoutTrailingSlash = substr($currentPath, 0, strlen($currentPath)-1);
        
        return $currentPathWithoutTrailingSlash;
    }
    
    public static function loadScripts(array $scripts, array $options = []) {
        foreach ($scripts as $handle => $script) {
            $deps = ['jquery'];
            if ($handle !== 'dragon-utils') {
                $deps[] = Config::$namespace . '-dragon-utils';
            }
            
            if (is_array($script)) {
                $deps = array_merge($deps, $script['deps']);
                $script = $script['script'];
            }
            
            $handle = Config::$namespace . '-' . $handle;
            wp_enqueue_script(
                $handle,
                static::getResourceUrl('js/' . $script),
                $deps,
                true,
                array_key_exists('in_footer', $options) ? $options['in_footer'] : true
                );
            
            if ((empty($options['allowAjaxOnFrontEnd']) || $options['allowAjaxOnFrontEnd']) &&
                static::$alreadySetAjaxUrl === false) {
                    wp_add_inline_script($handle, 'let ajax_url = "' . admin_url('admin-ajax.php') . '";', 'before');
                    static::$alreadySetAjaxUrl = true;
                }
                
        }
    }
    
    public static function loadScriptUrls(array $scriptUrls, bool $inFooter = true) {
        foreach ($scriptUrls as $handle => $script) {
            $handle = Config::$namespace . '-' . $handle;
            wp_enqueue_script($handle, $script, array('jquery'), true, $inFooter);
        }
    }
    
    public static function loadCss(array $cssFiles) {
        foreach ($cssFiles as $handle => $css) {
            $handle = Config::$namespace . '-' . $handle;
            wp_enqueue_style($handle, static::getResourceUrl('css/' . $css), [], true);
        }
    }
    
    public static function loadCssUrls(array $cssUrls) {
        foreach ($cssUrls as $handle => $css) {
            $handle = Config::$namespace . '-' . $handle;
            wp_enqueue_style($handle, $css, [], true);
        }
    }
    
    public static function urlToPath($url) {
        $wpRootUrl = Url::getDomain() . '/';
        $wpRootDirectory = ABSPATH;
        
        return str_replace($wpRootUrl, $wpRootDirectory, (string)$url);
    }
    
    public static function getUrlForPlugin($pluginDirectory) {
        $wpRootUrl = Url::getDomain() . '/';
        $wpRootDirectory = ABSPATH;
        $pluginPath = static::getPluginPath($pluginDirectory);
        
        return str_replace($wpRootDirectory, $wpRootUrl, (string)$pluginPath);
    }
    
    public static function getMimeType($filename) {
        return mime_content_type($filename);
    }
    
    public static function getLanguagePath() {
        return Config::$pluginDir . '/resources/lang/';
    }
    
    public static function saveBase64AsImageFile($data, $path, $name, $extension = null) {
        list($type, $image)	= explode(';', $data);
        list(, $mimeType)	= explode(':', $type);
        list(, $possibleExt)	= explode('/', $mimeType);
        list(, $imageData)	= explode(',', $data);
        $contents = base64_decode($imageData);
        
        $isImage = $extension !== null || in_array($possibleExt, ['png', 'jpg', 'gif', 'jpeg', 'bmp']);
        
        if ($isImage) {
            
            $extension = is_null($extension) ? $possibleExt : $extension;
            $file = $name . '.' . $extension;
            $fileFullPath = $path . $file;
            file_put_contents($fileFullPath, $contents);
            return $file;
            
        }
        
        return null;
    }
    
    private static function getResourceUrl(string $segment) {
        $path = Config::$pluginDir . '/resources/' . $segment;
        $url = Config::$pluginBaseUrl . 'resources/' . $segment;
        
        if (!file_exists($path)){
            $url = Config::$pluginBaseUrl . 'vendor/dragon-public/framework/resources/' . $segment;
        }
        
        return $url;
    }
    
    private static function includeFunction($functionName, $filePath) {
        if (!function_exists($functionName) && file_exists($filePath)) {
            require_once($filePath);
        }
    }
    
    private static function includeClassFile($className, $filePath) {
        if (!class_exists($className) && file_exists($filePath)) {
            require_once($filePath);
        }
    }
    
    private static function getPluginPath($pluginDirectory) {
        return WP_PLUGIN_DIR . '/' . $pluginDirectory . '/';
    }
}
