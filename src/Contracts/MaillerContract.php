<?php

namespace Dragon\Contracts;

interface MaillerContract {
	public function __construct();
	public function addToAddress(string $email, string $name);
	public function addFromAddress(string $from);
	public function addSubject(string $subject);
	public function addBody(string $body);
	public function getMaxRecipients() : ?int;
	public function send();
}
