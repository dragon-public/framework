<?php

namespace Dragon;

class ConfigFileNotFoundException extends \Exception{};

class Config {
	const DRAGON_CONFIG_NAME = 'dragon.php';
	
	public static ?string $namespace		= 'dragon';
	public static ?string $appNamespace		= 'Dragon\\';
	public static ?string $encryptionKey	= null;
	public static ?string $pluginBaseUrl	= '';
	public static ?string $pluginLoaderFile	= null;
	public static ?string $pluginDir		= null;
	
	private static array $dbConnections		= [];
	
	private static array $config = [];
	
	public static function init() {
		static::$pluginBaseUrl = plugin_dir_url(static::$pluginLoaderFile);
	}
	
	public static function setAppNamespace(string $namespace) {
		static::$appNamespace = $namespace;
	}
	
	public static function providers() {
		return ConfigFile::get('app', 'providers');
	}
	
	public static function save() {
		if (empty(static::$config)) {
			return;
		}
		
		$filename = static::getDragonConfigFilename();
		file_put_contents($filename, "<" . "?PHP\nreturn " . var_export(static::$config, true) . ";\n");
	}
	
	public static function open() {
		$filename = static::getDragonConfigFilename();
		$data = require_once($filename);
		if (is_array($data)) {
			static::$config = $data;
		}
	}
	
	public static function get(string $key) {
		return static::$config[$key] ?? null;
	}
	
	public static function set(string $key, string $value) {
		static::$config[$key] = $value;
	}
	
	public static function isDev() {
		return defined('WP_DEBUG') && WP_DEBUG;
	}
	
	public static function getDbConnections() {
		if (empty(static::$dbConnections)) {
			static::addDbConnections();
		}
		
		return static::$dbConnections;
	}
	
	public static function addDbConnections() {
		static::$dbConnections = ConfigFile::get('database', 'connections');
	}
	
	private static function getDragonConfigFilename() : ?string {
		$dir = Config::$pluginDir . '/../../dragon-config/';
		
		if (!is_dir($dir)) {
			mkdir($dir);
		}
		
		$relPath = $dir . static::DRAGON_CONFIG_NAME;
		$file = realpath($relPath);
		if (empty($file)) {
			file_put_contents($relPath, "<" . "?PHP\nreturn [];\n");
		}
		
		$file = realpath($relPath);
		if (empty($file)) {
			throw new ConfigFileNotFoundException("Could not find " . $relPath);
		}
		
		return $file;
	}
}
