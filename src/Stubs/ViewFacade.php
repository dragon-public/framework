<?php

namespace Dragon\Stubs;

use Illuminate\Support\Facades\View as LaravelView;

class ViewFacade extends LaravelView {
    public static function getParentFacadeAccessor() {
        return static::getFacadeAccessor();
    }
}
