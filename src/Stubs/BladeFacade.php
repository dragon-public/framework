<?php

namespace Dragon\Stubs;

use Illuminate\Support\Facades\Blade as LaravelBlade;

class BladeFacade extends LaravelBlade {
    public static function getParentFacadeAccessor() {
        return static::getFacadeAccessor();
    }
}
