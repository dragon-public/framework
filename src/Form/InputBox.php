<?php

namespace Dragon\Form;

class InputBox
{
	public static function create(array $data, bool $readonly = false)
	{
		if ($readonly) {
			return array_key_exists('value', $data['attributes']) ? $data['attributes']['value'] : null;
		}
		
		$out = '<input type="' . $data['type'] . '" ';
		
		if (!empty($data['attributes'])) {
			foreach ($data['attributes'] as $name => $value) {
				$value = @str_replace(
						["'", '"'],
						["&apos;", "&quot;"],
						(string)$value);
				$out .= $name . '="' . $value . '" ';
			}
		}
		
		$out .= '/>';
		
		return $out;
	}
}
