<?php

namespace Dragon\Form;

use Dragon\Input;

class DropDown
{
    const STATES = [
        "AL" => "Alabama",
        "AK" => "Alaska",
        "AZ" => "Arizona",
        "AR" => "Arkansas",
        "CA" => "California",
        "CO" => "Colorado",
        "CT" => "Connecticut",
        "DE" => "Delaware",
        "DC" => "District Of Columbia",
        "FL" => "Florida",
        "GA" => "Georgia",
        "HI" => "Hawaii",
        "ID" => "Idaho",
        "IL" => "Illinois",
        "IN" => "Indiana",
        "IA" => "Iowa",
        "KS" => "Kansas",
        "KY" => "Kentucky",
        "LA" => "Louisiana",
        "ME" => "Maine",
        "MD" => "Maryland",
        "MA" => "Massachusetts",
        "MI" => "Michigan",
        "MN" => "Minnesota",
        "MS" => "Mississippi",
        "MO" => "Missouri",
        "MT" => "Montana",
        "NE" => "Nebraska",
        "NV" => "Nevada",
        "NH" => "New Hampshire",
        "NJ" => "New Jersey",
        "NM" => "New Mexico",
        "NY" => "New York",
        "NC" => "North Carolina",
        "ND" => "North Dakota",
        "OH" => "Ohio",
        "OK" => "Oklahoma",
        "OR" => "Oregon",
        "PA" => "Pennsylvania",
        "RI" => "Rhode Island",
        "SC" => "South Carolina",
        "SD" => "South Dakota",
        "TN" => "Tennessee",
        "TX" => "Texas",
        "UT" => "Utah",
        "VT" => "Vermont",
        "VA" => "Virginia",
        "WA" => "Washington",
        "WV" => "West Virginia",
        "WI" => "Wisconsin",
        "WY" => "Wyoming",
    ];
    
    const FAKE_GROUP = 'Default';
    
    public static function create(array $data, $selectedValue = null, bool $readonly = false)
    {
        if ($readonly) {
            return empty($data['options'][$selectedValue]) ? null : $data['options'][$selectedValue];
        }
        
        $returnFullHtml = !empty($data['options']);
        $options = $data['options'] ?? $data;
        $out = '';
        
        if (!empty($data['attributes']['multiple'])) {
            $data['attributes']['name'] = $data['attributes']['name'] . '[]';
        }
        
        if ($returnFullHtml) {
            $out .= '<select ';
            foreach ($data['attributes'] as $key => $val) {
                $out .= $key . '="' . $val . '" ';
            }
            $out .= '>';
        }
        
        $input = new Input();
        $name = str_replace('[]', '', (string)$data['attributes']['name']);
        $selectedValues = empty($selectedValue) ? $input->post($name) : $selectedValue;
        $selectedValues = (array)$selectedValues;
        
        if (is_array(reset($options))) {
            foreach ($options as $group => $items) {
                $out .= '<optgroup label="' . $group . '">';
                $out .= static::makeOptions($items, $selectedValues);
                $out .= '</optgroup>';
            }
        } else {
            $out .= static::makeOptions($options, $selectedValues);
        }
        
        if ($returnFullHtml) {
            $out .= '</select>';
        }
        
        return $out;
    }
    
    public static function makeOptions(array $options, array $selectedValues) {
        $out = '';
        foreach ($options as $value => $text) {
            $selected = '';
            if (in_array($value, $selectedValues)) {
                $selected = 'selected';
            }
            
            $out .= '<option value="' . $value . '" ' . $selected . '>' . $text . '</option>';
        }
        
        return $out;
    }
}
