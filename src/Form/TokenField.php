<?php

namespace Dragon\Form;

use Dragon\View;
use Dragon\Input;
use Dragon\Blade;

class TokenField {
    public static function create(array $data, $value, bool $readonly = false) {
		$readOnlyOut = [];
		$input = new Input();
		$valueArray = $input->post($data['attributes']['name'], $value);
		
		if (!empty($valueArray) && !is_array($valueArray)) {
			$valueArray = json_decode($valueArray, true);
		}
		
		$tokenValues = [];
		if (is_array($valueArray)) {
			$itemRows = $data['model']::whereIn('id', $valueArray)->get();
			foreach ($itemRows as $itemRow) {
				$tokenValues[] = [
					"id"	=> $itemRow->id,
					"name"	=> $itemRow->{$data['name_column']},
					];
				$readOnlyOut[] = $itemRow->{$data['name_column']};
			}
		}
		
		if ($readonly) {
			return implode(', ', $readOnlyOut);
		}
		
		$newItems = 'false';
		if (array_key_exists('newItems', $data['attributes']) && (bool)$data['attributes']['newItems']) {
		    $newItems = 'true';
		}
		
		$items = (empty($data['attributes']['items']) ? [] : $data['attributes']['items']);
		return Blade::view('fields.token-field', [
		    'id'         	=> $data['attributes']['id'],
		    'name'         	=> $data['attributes']['name'],
		    'items'         => json_encode($items),
		    'new_items'     => $newItems,
			'value'			=> $tokenValues,
		]);
	}
}
