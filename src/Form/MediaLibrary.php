<?php

namespace Dragon\Form;

use Dragon\Blade;

class MediaLibrary {
    public static function create(array $data, $value) {
		wp_enqueue_media();
		$multiple = array_key_exists('multiple', $data['attributes']) && $data['attributes']['multiple'] ? 'true' : 'false';
		return Blade::view('fields.media-library', [
		    'html_id'	=> $data['attributes']['id'],
		    'value'			=> $value,
		    'multiple'      => $multiple,
		]);
	}
}
