<?php

namespace Dragon\Form;

class TextArea
{
    public static function create(array $data, string $value = null, bool $readonly = false)
    {
        if ($readonly) {
            return $value;
        }
        
        $out = '<textarea ';
        
        if (!empty($data['attributes'])) {
            foreach ($data['attributes'] as $key => $val) {
                $val = str_replace(
                    ["'", '"'],
                    ["&apos;", "&quot;"],
                    (string)$val);
                $out .= $key . '="' . $val . '" ';
            }
        }
        
        $out .= '>' . $value . '</textarea>';
        
        return $out;
    }
}
