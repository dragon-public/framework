<?php

namespace Dragon\Form;

class Checkbox
{
    public static function create(array $data, bool $currentValue, bool $readonly = false)
    {
        $checked = !empty($currentValue);
        if ($readonly) {
            return $checked ? 'Checked' : 'Unchecked';
        }
        
        $out = '<input type="checkbox" ';
        
        if ($checked) {
            $out .= 'checked="checked" ';
        }
        
        if (!empty($data['attributes'])) {
            foreach ($data['attributes'] as $name => $value) {
                $value = str_replace(
                    ["'", '"'],
                    ["&apos;", "&quot;"],
                    (string)$value);
                $out .= $name . '="' . $value . '" ';
            }
        }
        
        $out .= '/>';
        
        return $out;
    }
}
