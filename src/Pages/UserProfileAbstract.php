<?php

namespace Dragon\Pages;

use Dragon\Traits\HasHooks;
use Dragon\View;
use Dragon\User;
use Dragon\Blade;

abstract class UserProfileAbstract extends AdminOptionsAbstract {
	use HasHooks;
	
	protected $settingsAreGlobal = false;
	protected $adminForm = false;
	protected $saveButton = "submit";
	
	protected array $sections = [
		// 		'Section Title' => [
		// 			'field_id'	=> [
		// 				'required-role'	=> 'administrator', // Optional
		// 				'required-capability'	=> 'read', // Optional
		// 				'type'		=> 'select',
		// 				'required'	=> true,
		// 				'label'		=> 'Just a dropdown',
		// 				'options'	=> [
		// 					null => 'None',
		// 				],
		// 			],
		// 		],
	];
	
	public function render() {
		$this->setSections();
		$this->setup();
		$this->pageData['sections'] = Blade::view('table/section-rows', $this->pageData);
		echo Blade::view('user-profile-fields', $this->pageData);
	}
	
	public static function display(\WP_User $user) {
		$instance = new static();
		$instance->userId = $user->ID;
		$instance->render();
	}
	
	public static function init() {
		static::$actions = [
			'show_user_profile'	=> [
				[static::class, 'display'],
			],
			'edit_user_profile'	=> [
				[static::class, 'display'],
			],
			'personal_options_update'	=> [
				[static::class, 'saveFields'],
			],
			'edit_user_profile_update'	=> [
				[static::class, 'saveFields'],
			],
		];
		
		static::setActions();
	}
	
	public static function saveFields(int $userId) {
		$instance = new static();
		$instance->userId = $userId;
		$instance->saveAll();
	}
	
	protected function setup() {
		$this->pageData['userId'] = $this->userId;
		$this->removeFieldsIfNotOfRequiredRole();
		parent::setup();
	}
	
	protected function saveAll() {
		$this->setup();
		$this->saveSettings($this->pageData);
	}
	
	private function removeFieldsIfNotOfRequiredRole() {
		$roles = array_keys(User::getRoles());
		$capabilities = array_keys(User::getCapabilities());
		foreach ($this->sections as $title => $fields) {
			foreach ($fields as $fieldIndex => $field) {
				if (!empty($field['required-role']) &&
					!$this->hasPermission($roles, $field['required-role'])
					) {
						unset($this->sections[$title][$fieldIndex]);
					}
					
					if (!empty($field['required-capability']) &&
						!$this->hasPermission($capabilities, $field['required-capability'])
						) {
							unset($this->sections[$title][$fieldIndex]);
						}
			}
			
			if (empty($this->sections[$title])) {
				unset($this->sections[$title]);
			}
		}
	}
	
	private function hasPermission(array $givenPermissions, $requestedPermission) : bool {
		if (in_array('administrator', $givenPermissions)) {
			return true;
		}
		
		$requestedPermission = is_array($requestedPermission) ? $requestedPermission : [$requestedPermission];
		foreach ($requestedPermission as $permission) {
			if (in_array($permission, $givenPermissions)) {
				return true;
			}
		}
		
		return false;
	}
}
