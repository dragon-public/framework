<?php

namespace Dragon\Pages;

use Dragon\Config;
use Dragon\Abstracts\OptionsAbstract;
use Dragon\Blade;

abstract class AdminOptionsAbstract extends OptionsAbstract {
    protected $saveButton = null;
    protected bool $shouldSanitize = false;
    
    protected array $sections = [
        'Plugin Settings' => [
            'remove_tables_on_deactivation'	=> [
                'type'		=> 'select',
                'required'	=> true,
                'label'		=> 'Remove all plugin tables on deactivation?',
                'options'	=> [
                    'yes'	=> 'Yes',
                    'no'	=> 'No (Recommended)',
                ],
            ],
        ],
    ];
    
    public function render() {
        $this->setSections();
        $this->setup();
        $this->pageData += [
            'userId' => $this->userId,
            'title' => get_admin_page_title(),
            'nonce' => $this->makeNonce(),
            'prefix' => Config::$namespace,
        ];
        
        $this->pageData['rows'] = Blade::view('table/section-rows', $this->pageData);
        
        echo Blade::view('admin-options', $this->pageData);
    }
    
    protected function setSections() {
        //
    }
    
    protected function setup() {
        $this->saveButton = empty($this->saveButton) ? Config::$namespace . '_save_settings' : $this->saveButton;
        $this->pageData['sections'] = $this->sections;
        
        $this->setRequiredAndEncryptedFields();
        $this->saveSettings($this->pageData);
    }
    
    private function setRequiredAndEncryptedFields() {
        foreach ($this->sections as $fields) {
            foreach ($fields as $fieldName => $fieldData) {
                if (!empty($fieldData['required'])) {
                    $this->requiredFields[] = $fieldName;
                } else {
                    $this->optionalFields[] = $fieldName;
                }
                
                if (!empty($fieldData['encrypted'])) {
                    $this->encryptedFields[] = $fieldName;
                }
            }
        }
    }
}
