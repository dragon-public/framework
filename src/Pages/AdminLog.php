<?php

namespace Dragon\Pages;

use Dragon\Config;
use Dragon\Abstracts\PageAbstract;
use Dragon\Blade;

class AdminLog extends PageAbstract {
    public function render() {
        $this->handleClear();
        
        $pageData = [];
        $pageData['log'] = file_get_contents(Config::$pluginDir . '/plugin.log');
        echo Blade::view('admin-log', $pageData);
    }
    
    private function handleClear() {
        if ($this->input->hasPost('dragon_clear_log')) {
            file_put_contents(Config::$pluginDir . '/plugin.log', '');
        }
    }
}
