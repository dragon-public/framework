<?php

namespace Dragon\Models;

use Illuminate\Database\Eloquent\Model;

class PseudoSession extends Model {
    public $timestamps = false;
    protected $table = 'dragon_pseudo_sessions';
    
    protected $casts = [
    	'expiry' => 'timestamp',
    ];
    
    protected $fillable = [
    	'token',
    	'key',
    	'value',
    	'expiry',
    ];
}
