<?php

namespace Dragon\Models;

use Illuminate\Database\Eloquent\Model;

class Migration extends Model {
    public $timestamps = false;
    protected $table = 'dragon_migrations';
    protected $fillable = ['migration'];
}
