<?php

namespace Dragon\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmailMessage extends Model {
    use SoftDeletes;
    
    public $timestamps = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dragon_email_messages';
}
