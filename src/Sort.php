<?php

namespace Dragon;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

class Sort {
    const SORT_ORDER_ASCENDING = 'ASC';
    const SORT_ORDER_DESCENDING = 'DESC';
    
    private string $defaultOrder = 'ASC';
    private ?Input $input = null;
    private array $mapQueryToColumns = [];
    
    public function __construct(array $mapQueryToColumns, string $defaultOrder = 'ASC') {
        $this->input = new Input();
        $this->defaultOrder = $defaultOrder;
        $this->mapQueryToColumns = $mapQueryToColumns;
    }
    
    public function maybeSetFromUrl(&$sortable) {
        $sortBy = $this->input->get('sortby');
        $sortOrder = $this->input->get('sortorder', $this->defaultOrder);
        
        if (empty($sortBy) || empty($this->mapQueryToColumns[$sortBy])) {
            return false;
        }
        
        $this->sort($sortable, $this->mapQueryToColumns[$sortBy], $sortOrder);
        return true;
    }
    
    public function sort(&$sortable, string $column, string $order = null) {
        $order = $order ?? $this->defaultOrder;
        
        if ($sortable instanceOf Builder) {
            $sortable = $sortable->OrderBy($column, $order);
        } elseif ($sortable instanceOf Collection) {
            $sortable = $sortable->sortBy(function (Model $item, $key) use ($column) {
                return $item->{$column};
            }, SORT_REGULAR, $order === static::SORT_ORDER_DESCENDING);
        }
    }
    
    public function getSortBy() {
        return $this->input->get('sortby');
    }
    
    public function getOrder(string $sortBy) {
        $sortByQuery = $this->input->get('sortby');
        if ($sortByQuery !== $sortBy) {
            return $this->defaultOrder;
        }
        
        return $this->input->get('sortorder', $this->defaultOrder);
    }
    
    public function getLinkFor(string $sortBy) {
        $currentOrder = $this->getOrder($sortBy);
        $newOrder = static::SORT_ORDER_ASCENDING;
        if ($currentOrder === static::SORT_ORDER_ASCENDING) {
            $newOrder = static::SORT_ORDER_DESCENDING;
        }
        
        return Url::getCurrentUrl(['sortby' => $sortBy, 'sortorder' => $newOrder]);
    }
}
