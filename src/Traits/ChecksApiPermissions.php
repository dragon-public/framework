<?php

namespace Dragon\Traits;

use Dragon\Config;

trait ChecksApiPermissions {
	private static function currentUserCan(string $operation) {
		if (!current_user_can($operation)) {
			return new \WP_Error(
				Config::$namespace . '_rest_cannot_' . $operation,
				'Ah, ah, ah! You didn\'t say the magic word. Ah, ah, ah!',
				['status' => rest_authorization_required_code()]
				);
		}
		
		return true;
	}
}
