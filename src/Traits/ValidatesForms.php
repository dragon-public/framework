<?php

namespace Dragon\Traits;

use Dragon\Config;

if (!class_exists('FormInvalidNonceException')) {
    class FormInvalidNonceException extends \Exception{}
    class FormMissingRequiredFieldsException extends \Exception{}
}

trait ValidatesForms {
    protected array $pageData = [];
    protected string $button = 'dragon_submit';
    protected array $requiredFields = [];
    protected string $badNonceMessage = 'An invalid nonce was supplied for the form. Try refreshing the page and reentering your information.';
    protected string $missingRequiredMessage = 'One or more required fields are missing. Please enter the missing information and try again.';
    protected string $badCaptchaMessage = 'The Captcha field was either incorrect or expired. Please try again.';
    protected string $genericError = 'An error occurred.';
    protected ?string $missingField = null;
    
    
    protected function handleSubmission() {
        if ($this->input->hasPost($this->button)) {
            try {
                
                $this->throwIfBadNonce(Config::$namespace . "_nonce");
                $this->throwIfMissingRequiredFields();
                return true;
                
            } catch (FormInvalidNonceException $e) {
                
                $this->handleBadNonce();
                return false;
                
            } catch (FormMissingRequiredFieldsException $e) {
                
                $this->handleMissingRequired();
                return false;
                
            }
            
            return $this->extraSubmissionValidation();
        } else {
            return false;
        }
    }
    
    protected function extraSubmissionValidation() {
        return true;
    }
    
    protected function handleBadNonce() {
        $this->makeErrorNotice($this->badNonceMessage);
    }
    
    protected function handleMissingRequired() {
        $this->makeErrorNotice($this->missingRequiredMessage);
    }
    
    private function throwIfMissingRequiredFields() {
        if ($this->areAllRequiredFieldsPresent() === false) {
            throw new FormMissingRequiredFieldsException();
        }
    }
    
    private function areAllRequiredFieldsPresent() {
        foreach ($this->requiredFields as $field) {
            
            $postNotSet = empty($this->input->post($field));
            $fileNotSet = array_key_exists($field, $_FILES) === false || $_FILES[$field]['tmp_name'] === '';
            
            if ($postNotSet && $fileNotSet) {
                
                $this->missingField = $field;
                return false;
                
            }
            
        }
        
        return true;
    }
    
    private function throwIfBadNonce($pageNamespace) {
        if ($this->isValidNonce($pageNamespace) === false) {
            throw new FormInvalidNonceException();
        }
    }
    
    private function isValidNonce($pageNamespace) {
        return empty($this->input->post($pageNamespace)) ? false : wp_verify_nonce($this->input->post($pageNamespace), $pageNamespace);
    }
}
