<?php

namespace Dragon\Traits;

trait HasHooks {
    protected static $actions = [
        //
    ];
    
    protected static $filters = [
        //
    ];
    
    protected static function setActions() {
        static::setHooks(static::$actions, 'add_action');
    }
    
    protected static function setFilters() {
        static::setHooks(static::$filters, 'add_filter');
    }
    
    private static function setHooks(array &$hooks, $callbackName) {
        foreach ($hooks as $hook => $callbacks) {
            
            foreach ($callbacks as $callbackData) {
                
                $callback = empty($callbackData['callback']) ? $callbackData : $callbackData['callback'];
                $priority = empty($callbackData['priority']) ? 10 : $callbackData['priority'];
                $args = empty($callbackData['args']) ? 1 : $callbackData['args'];
                call_user_func($callbackName, $hook, $callback, $priority, $args);
                
            }
            
        }
    }
}
