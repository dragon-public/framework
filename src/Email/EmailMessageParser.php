<?php

namespace Dragon\Email;

use Dragon\Config;

class EmailMessageParser {
	public static function makeTemplateFor(string $templateName, ?string $jsonData = null) : ?string {
		$filename = Config::$pluginDir . '/resources/views/emails/' . $templateName . '.html';
		if (!file_exists($filename)) {
			return null;
		}
		
		$rawTemplate = file_get_contents($filename);
		$data = empty($jsonData) ? [] : json_decode($jsonData, true);
		return static::replaceVariables($rawTemplate, $data);
	}
	
	private static function replaceVariables(string $template, array $templateData) {
		$out = '';
		
		$open = false;
		for ($i=0; $i<strlen($template); $i++) {
			if ($template[$i] === '#' && $template[$i+1] === '#') {
				$open = !$open;
				$i++;
				continue;
			}
			
			if ($open) {
				$end = strpos($template, '#', $i)-1;
				$var = substr($template, $i, $end-$i+1);
				$value = $templateData[$var] ?? '';
				$out .= '<textarea name="' . $var . '" class="dragon-email-textarea">' . $value . '</textarea>';
				$i = $end;
			} else {
				$out .= $template[$i];
			}
		}
		
		return $out;
	}
}
