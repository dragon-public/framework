<?php

namespace Dragon\Email;

use Dragon\Log;

class Email {
    public static function logFailedMailings(\WP_Error $error) {
        (new Log())->info($error->error_data);
        (new Log())->info($error->errors);
    }
    
    protected static function batchSend(array $to, string $subject, string $message) {
        foreach ($to as $email) {
            static::send($email, $subject, $message);
        }
    }
    
    protected static function send($to, $subject, $message, $headers = [], $attachmentPaths = []) {
        if (WP_DEBUG) {
            $log = new Log();
            $log->info($to);
            $log->info($subject);
            $log->info($message);
        }
        
        return wp_mail($to, $subject, $message, $headers, $attachmentPaths);
    }
}
