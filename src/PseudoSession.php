<?php

namespace Dragon;

use Dragon\Models\PseudoSession as PseudoSessionModel;
use Carbon\Carbon;

class PseudoSession {
	const COOKIE_NAME = 'APP_SESS_ID';
	const EXPIRE_ONE_DAY = 24*60*60;
	
	public static function start(bool $httpsOnly = false, bool $blockJs = true) {
		if (empty($_COOKIE[static::COOKIE_NAME])) {
			static::setTokenCookie(static::getDefaultExpiry(), $httpsOnly, $blockJs);
			static::gc();
		}
	}
	
	public static function set(string $key, string $data, int $expirySeconds = 0) {
		$token = static::getToken();
		if (empty($token)) {
			return;
		}
		
		$expiry = empty($expirySeconds) ? static::getDefaultExpiry() : (time()+$expirySeconds);
		PseudoSessionModel::updateOrCreate([
			'token'	=> $token,
			'key'	=> $key,
		], [
			'value'		=> $data,
			'expiry'	=> Carbon::parse($expiry),
		]);
	}
	
	public static function get(string $key, string $default = null) {
		$token = static::getToken();
		if (empty($token)) {
			return $default;
		}
		
		$row = PseudoSessionModel::where('token', static::getToken())
			->where('key', $key)
			->where('expiry', '>=', Carbon::now())
			->first();
		
		return empty($row) ? $default : $row->value;
	}
	
	public static function delete(string $key) {
		$token = static::getToken();
		if (empty($token)) {
			return;
		}
		
		PseudoSessionModel::where('token', static::getToken())
		->where('key', $key)
		->delete();
	}
	
	public static function destroy() {
		$token = static::getToken();
		if (empty($token)) {
			return;
		}
		
		PseudoSessionModel::where('token', static::getToken())->delete();
		static::setTokenCookie(time() - 3600);
	}
	
	public static function gc() {
		PseudoSessionModel::where('expiry', '<', Carbon::now())->delete();
	}
	
	private static function setTokenCookie(int $expiry = 0, bool $httpsOnly = true, bool $blockJs = true) {
		$token = static::makeToken();
		$expiry = empty($expiry) ? static::getDefaultExpiry() : $expiry;
		$success = setcookie(static::COOKIE_NAME, $token, $expiry, '/', $_SERVER['HTTP_HOST'], $httpsOnly, $blockJs);
		if ($success) {
			$_COOKIE[static::COOKIE_NAME] = $token;
		}
	}
	
	private static function makeToken() {
		return hash('sha256', openssl_random_pseudo_bytes(32));
	}
	
	private static function getToken() {
		return empty($_COOKIE[static::COOKIE_NAME]) ? null : $_COOKIE[static::COOKIE_NAME];
	}
	
	private static function getDefaultExpiry() {
		return (time()+24*60*60);
	}
}
