<?php

namespace Dragon;

/**
 * @deprecated Use Blade instead.
 */
class View {
    public static function displayPage($page, array $pageData = array()) {
        $file = static::getFilenameForPage($page);
        if (!file_exists($file)) {
            echo __("Template not found: " .$page, Config::$namespace);
        } else {
            require($file);
        }
    }
    
    public static function parsePage($page, array $pageData = array()) {
        $callback = function () use ($page, $pageData) {
            View::displayPage($page, $pageData);
        };
        
        return static::collectOutput($callback);
    }
    
    public static function parseTemplate($page, array $pageData = array()) {
        $file = static::getFilenameForPage($page);
        $page = file_get_contents($file);
        
        $template = array_keys($pageData);
        $replacements = array_values($pageData);
        $page = str_replace($template, $replacements, (string)$page);
        
        return $page;
    }
    
    private static function collectOutput(Callable $callback) {
        $oldBuffer = ob_get_contents();
        ob_clean();
        $callback();
        $newContent = ob_get_contents();
        ob_clean();
        echo $oldBuffer;
        return $newContent;
    }
    
    private static function getViewPath() {
        return realpath(Config::$pluginDir . '/resources/views');
    }
    
    private static function getFilenameForPage($page) : string {
        $root = static::getViewPath();
        $filename = $root . '/' . $page . '.php';
        
        if (!file_exists($filename)) {
            $filename = realpath(__DIR__ . '/../resources/views/' . $page . '.php');
        }
        
        return $filename;
    }
}
