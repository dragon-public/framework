<?php

namespace Dragon;

use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class Paging {
	public $paginationQueryVar = 'paged';
	
	private ?Input $input = null;
	private $paging = [
		'per_page'	=> 20,
	];
	
	public function __construct(int $perPage = 20, string $paginationQueryVar = 'paged') {
		$this->input = new Input();
		$this->setPaginator();
		
		$this->paging['per_page'] = $perPage;
		$this->paginationQueryVar = $paginationQueryVar;
	}
	
	public function addPaging(&$pageable) {
		$pagingData = [];
		
		if ($pageable instanceOf Builder) {
			
			$pageable = $pageable->paginate($this->paging['per_page'])
			->setPageName($this->paginationQueryVar)
			->withPath(Url::getCurrentUrl());
			
			$pagingData = $pageable->toArray();
			unset($pagingData['data']);
			
		} elseif ($pageable instanceOf Collection) {
			$currentPage = $this->input->get($this->paginationQueryVar, 1);
			$total = $pageable->count();
			$perPage = $this->paging['per_page'];
			
			$pageable = $pageable->forPage($currentPage, $perPage);
			
			$pagingParam = $this->paginationQueryVar;
			$lastPage = (ceil($total/$perPage));
			$pagingData = [
				'pagingVar'		=> $this->paginationQueryVar,
				'totalRows'		=> $total,
				'last_page'		=> (int)$lastPage,
				'path'			=> Url::getCurrentUrl(),
				'current_page'	=> (int)$currentPage,
				'first_page_url'	=> Url::getCurrentUrl([$pagingParam => 1]),
				'prev_page_url'		=> Url::getCurrentUrl([$pagingParam => max($currentPage-1, 1)]),
				'next_page_url'		=> Url::getCurrentUrl([$pagingParam => min($currentPage+1, $lastPage)]),
				'last_page_url'		=> Url::getCurrentUrl([$pagingParam => $lastPage]),
			];
		}
		
		$this->paging = $pagingData;
	}
	
	public function getPaging() {
		return $this->paging;
	}
	
	public function getTotalPages(int $default) {
		return empty($this->paging['total']) ? $default : $this->paging['total'];
	}
	
	private function setPaginator() {
		Paginator::currentPageResolver(function() {
			return (int)$this->input->get($this->paginationQueryVar, 1);
		});
	}
}
