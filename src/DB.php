<?php

namespace Dragon;

use Illuminate\Database\Capsule\Manager as Capsule;
use Dragon\Models\Migration;
use Dragon\Abstracts\MigrationAbstract;

class DB {
	const MIGRATION_TABLE_NAME = 'dragon_migrations';
	
	const DRAGON_TABLE_NAMES = [
		'migrations',
		'pseudo_sessions',
	];
	
	private $multisiteNonPrefixedTables = [
		'blogmeta', 'blogs', 'blog_versions',
		'registration_log', 'signups', 'site',
		'sitemeta', 'usermeta', 'users',
	];
	
	private static $instance = null;
	private $migrationDir = null;
	private $dragonMigrationDir = null;
	
	public static function make() {
		if (empty(static::$instance)) {
			static::$instance = new static();
		}
		
		return static::$instance;
	}
	
	public function migrate() {
		$this->ensureMigrationsTableExists();
		$this->ensureDragonTablesExist();
		
		$types = [
			MigrationAbstract::TYPE_CREATE_TABLE,
			MigrationAbstract::TYPE_ADD_COLUMN,
			MigrationAbstract::TYPE_CHANGE_COLUMN,
		];
		
		foreach ($types as $type) {
			
			$this->iterateMigrations($this->migrationDir, function (MigrationAbstract $migrationClass, $migrationName) use ($type) {
				if ($migrationClass->type === $type &&
						Migration::where('migration', $migrationName)->exists() === false) {
							
							$migrationClass->up();
							Migration::create([
								'migration'	=> $migrationName,
							]);
							
						}
			});
				
		}
	}
	
	public function rollback() {
		$this->rollbackTables($this->migrationDir);
		$this->rollbackTables($this->dragonMigrationDir);
		
		$this->migrationsTable()->down();
	}
	
	public function hasTable($tableName) {
		return Capsule::schema()->hasTable($tableName);
	}
	
	private function rollbackTables(string $dir) {
		$this->iterateMigrations($dir, function (MigrationAbstract $migrationClass) {
			
			if (strpos(get_class($migrationClass), 'CreateMigrationsTable') !== false) {
				return;
			}
			
			if ($this->hasTable($migrationClass->tableName)) {
				$migrationClass->down();
			}
			
		});
	}
	
	private function __construct() {
		$capsule = new Capsule();
		
		foreach (Config::getDbConnections() as $connectionName => $connectionData) {
			$capsule->addConnection($connectionData, $connectionName);
		}
		
		$capsule->setAsGlobal();
		$capsule->bootEloquent();
		
		$this->setMigrationsDirectory();
	}
	
	private function ensureMigrationsTableExists() {
		if ($this->hasTable(static::MIGRATION_TABLE_NAME) === false) {
			$this->migrationsTable()->up();
		}
	}
	
	private function ensureDragonTablesExist() {
		$this->iterateMigrations($this->dragonMigrationDir, function (MigrationAbstract $migrationClass, $migrationName) {
			if (strpos(get_class($migrationClass), 'CreateMigrationsTable') !== false) {
				return;
			}
			
			if (Migration::where('migration', $migrationName)->exists() === false) {
				$migrationClass->up();
				Migration::create([
					'migration'	=> $migrationName,
				]);
			}
		});
	}
	
	private function migrationsTable() {
		require_once(realpath(__DIR__ . '/../migrations/create_migrations_table.php'));
		if (class_exists('CreateMigrationsTable')) {
			return new \CreateMigrationsTable();
		}
		
		return new \DRAGONDEPS_CreateMigrationsTable();
	}
	
	private function setMigrationsDirectory() {
		$pluginFile = Config::$pluginLoaderFile;
		$this->migrationDir = dirname($pluginFile) . '/migrations/';
		$this->dragonMigrationDir = __DIR__ . '/../migrations/';
	}
	
	private function iterateMigrations(string $dirName, Callable $callback) {
		$dir = dir($dirName);
		while (false !== ($entry = $dir->read())) {
			
			if (in_array($entry, ['.', '..']) || strpos($entry, '.php') === false) {
				continue;
			}
			
			$filename = $dirName . $entry;
			require_once($filename);
			$migrationName = str_replace('.php', '', (string)$entry);
			$classname = str_replace('_', '', (string)$migrationName);
			
			$callback(new $classname(), $migrationName);
			
		}
		
		$dir->close();
	}
}
