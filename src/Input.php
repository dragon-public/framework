<?php

namespace Dragon;

class Input {
	private $postRaw = null;
	private $getRaw = null;
	
	public function __construct() {
		$this->postRaw = array_map('stripslashes_deep', $_POST);
		$this->getRaw = array_map('stripslashes_deep', $_GET);
	}
	
	public function header(string $key, ?string $default = null) {
		$keyName = 'HTTP_' . str_replace('-', '_', strtoupper($key));
		return array_key_exists($keyName, $_SERVER) ? $_SERVER[$keyName] : $default;
	}
	
	public function file(string $key, bool $onlyPath = true) {
		if (empty($_FILES[$key])) {
			return null;
		}
		
		return $onlyPath ? $_FILES[$key]['tmp_name'] : $_FILES[$key];
	}
	
	public static function getFilesKey() {
		return static::$filesKey;
	}
	
	public function json(bool $raw = false) {
		$input = file_get_contents("php://input");
		if (empty($input)) {
			return null;
		}
		
		return $raw ? $input : json_decode($input);
	}
	
	public function post($key = null, $default = null) {
		if (empty($key)) {
			return $this->postRaw;
		}
		
		return !array_key_exists($key, $this->postRaw) ? $default : $this->postRaw[$key];
	}
	
	public function get($key = null, $default = null) {
		if (empty($key)) {
			return $this->getRaw;
		}
		
		return !array_key_exists($key, $this->getRaw) ? $default : $this->getRaw[$key];
	}
	
	public function hasPost($key) {
		return $this->hasKey($key, $this->postRaw);
	}
	
	public function hasGet($key) {
		return $this->hasKey($key, $this->getRaw);
	}
	
	public function removePostItem(string $key) {
		unset($this->postRaw[$key]);
	}
	
	public function removeGetItem(string $key) {
		unset($this->getRaw[$key]);
	}
	
	public function setCookie(string $key, string $value, string $expires = "+30 days") {
		setcookie($key, $value, strtotime($expires));
	}
	
	private function hasKey($key, array $rawData) {
		if (is_array($key)) {
			foreach ($key as $item) {
				if ($this->hasKey($item, $rawData)) {
					return true;
				}
			}
			
			return false;
		}
		
		return array_key_exists($key, $rawData);
	}
}
