<?php

namespace Dragon;

use Dragon\Abstracts\ArrayDotFileAbstract;

class ConfigFile extends ArrayDotFileAbstract {
    public static function getConfigFile(string $name) : array {
        $file = realpath(static::getFileBasePath() . '/' . $name . '.php');
        return empty($file) ? [] : require($file);
    }
    
    protected static function getFileBasePath() {
        $path = realpath(__DIR__ . '/../../../../config');
        return empty($path) ? null : $path . '/';
    }
}
