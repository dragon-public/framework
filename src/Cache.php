<?php

namespace Dragon;

class Cache {
	public static function set($key, $value, $expirationTimeStamp = null) {
		$cacheThis = [
				'expires'	=> $expirationTimeStamp,
				'data'		=> $value,
		];
		
		$cacheKey = static::getNamespacedCacheKey($key);
		static::{'saveTo' . static::getDriverType()}($cacheKey, $cacheThis);
	}
	
	public static function get($key, $default = null) {
		$cacheKey = static::getNamespacedCacheKey($key);
		
		$cachedData = static::{'getFrom' . static::getDriverType()}($cacheKey);
		if (!is_array($cachedData)) {
			return $default;
		}
		
		$expires = $cachedData['expires'];
		if ($expires !== null && (int)$expires < time()) {
			return $default;
		}
		
		return $cachedData['data'];
	}
	
	public static function delete($key) {
		$cacheKey = static::getNamespacedCacheKey($key);
		static::{'deleteFrom' . static::getDriverType()}($cacheKey);
	}
	
	private static function getNamespacedCacheKey($key) {
		return 'dragon-cache.' . Config::$namespace . '.' . $key;
	}
	
	private static function getDriverType() {
		$driver = ConfigFile::get('cache', 'driver');
		return empty($driver) ? 'db' : $driver;
	}
	
	private static function saveToDb(string $cacheKey, array $cacheThis) {
		update_option($cacheKey, $cacheThis);
	}
	
	private static function getFromDb(string $cacheKey) {
		return get_option($cacheKey, null);
	}
	
	private static function deleteFromDb(string $cacheKey) {
		delete_option($cacheKey);
	}
	
	private static function saveToFile(string $cacheKey, array $cacheThis) {
		$file = static::getFullFilePath($cacheKey);
		file_put_contents($file, json_encode($cacheThis));
	}
	
	private static function getFromFile(string $cacheKey) {
		$file = static::getFullFilePath($cacheKey);
		if (file_exists($file) === false) {
			return null;
		}
		$contents = file_get_contents($file);
		return json_decode($contents, true);
	}
	
	private static function deleteFromFile(string $cacheKey) {
		$file = static::getFullFilePath($cacheKey);
		unlink($file);
	}
	
	private static function getFullFilePath(string $key) {
		$dragonConfig = realpath(Config::$pluginDir . '/../../dragon-config');
		return $dragonConfig . '/' . $key . '.cache';
	}
}
