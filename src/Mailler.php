<?php

namespace Dragon;

use Dragon\Contracts\MaillerContract;

abstract class Mailler {
	protected ?MaillerContract $mailler = null;
	
	public function __construct() {
		$this->setMailler();
	}
	
	public function __call(string $method, array $params) {
		return call_user_func_array([$this->mailler, $method], $params);
	}
	
	abstract protected function setMailler();
}
