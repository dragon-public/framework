<?php

namespace Dragon;

use DragonDeps\Dependencies\GuzzleHttp\Client;

class Captcha {
	const VERIFY_URL = 'https://www.google.com/recaptcha/api/siteverify';
	
	private $input = null;
	private $secretKey = null;
	private $remoteIp = null;
	private $guzzle = null;
	
	public function __construct() {
		$this->input = new Input();
		$this->secretKey = dragonDecrypt(get_option('dragon_recaptcha_secret_key', null));
		$this->setRemoteIp();
		$this->guzzle = new Client();
	}
	
	public function isValid() {
		$responseToken = $this->input->post('g-recaptcha-response');
		return !($responseToken === null || $this->isValidAtGoogle($responseToken) === false);
	}
	
	private function setRemoteIp() {
		$this->remoteIp = $_SERVER['REMOTE_ADDR'];
	}
	
	private function isValidAtGoogle($responseToken) {
		$response = $this->guzzle->post(static::VERIFY_URL, [
			'form_params' => [
				'secret'	=> $this->secretKey,
				'response'	=> $responseToken,
				'remoteip'	=> $this->remoteIp,
			],
		]);
		$response = json_decode((string)$response->getBody());
		return $response->success;
	}
}
