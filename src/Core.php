<?php

namespace Dragon;

class Core {
    public static function init() {
        static::ensureCorrectDir();
        Config::init();
        Log::init();
        Ignite::fire();
        
        $providers = Config::providers();
        static::loadProviders($providers['global']);
        
        if (is_admin()) {
            static::loadProviders($providers['admin_only']);
            
            $adminPluginHooks = $providers['on_demand']['AdminPluginHooks'];
            register_activation_hook(Config::$pluginLoaderFile, [$adminPluginHooks, 'onActivation']);
            register_deactivation_hook(Config::$pluginLoaderFile, [$adminPluginHooks, 'onDeactivation']);
            
            add_action('init', [$adminPluginHooks, 'init']);
            
        } else {
            static::loadProviders($providers['frontend_only']);
            add_action('init', array($providers['on_demand']['FrontEndPluginHooks'], 'init'));
        }
    }
    
    private static function loadProviders(array $providers) {
        foreach ($providers as $provider) {
            $provider::init();
        }
    }
    
    private static function ensureCorrectDir() {
        $path = explode('/', __DIR__);
        if (!in_array('siteworks', $path)) {
            throw new \Exception("Siteworks plugin was renamed. This could be " .
                "a security issue. Please change it back.");
        }
    }
}
