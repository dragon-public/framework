<?php

namespace Dragon;

use Carbon\Carbon;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;

class Log implements LoggerInterface {
	use LoggerTrait;
	
	public static function init() {
		if (!file_exists(Config::$pluginDir . '/plugin.log')) {
			touch(Config::$pluginDir . '/plugin.log');
		}
	}
	
	public function log($level, $message, array $context = []) {
		$data = (array)$message;
		
		if (empty($data)) {
			$data[] = $message;
		}
		
		$item = $this->makePrintable($data);
		
		if (!empty($context)) {
			$item .= " CONTEXT: " . print_r($context, true);
		}
		
		$dateTime = '[' . Carbon::now()->toDateTimeString() . '] ' . $_SERVER['REMOTE_ADDR'] . ' ' . strtoupper($level);
		file_put_contents(
				Config::$pluginDir . '/plugin.log',
				$dateTime . ' ' . $item . "\n\n",
				FILE_APPEND
				);
	}
	
	private function makePrintable($data) {
		$data = is_array($data) ? print_r($data, true) : $data;
		$data = is_null($data) ? 'NULL' : $data;
		$data = ($data === '') ? 'string(0)' : $data;
		$data = is_object($data) ? 'object(' . get_class($data) . ')' : $data;
		
		if (is_bool($data)) {
			$data = ($data === true) ? 'bool(true)' : 'bool(false)';
		}
		
		return $data;
	}
}
