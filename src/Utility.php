<?php

namespace Dragon;

class Utility {
    public static function onlyDigits(string $text) : string {
        $out = "";
        
        for ($i=0; $i<strlen($text); $i++) {
            if (ctype_digit($text[$i])) {
                $out .= $text[$i];
            }
        }
        
        return $out;
    }
    
    public static function pick(string $key, array $array) {
        return array_key_exists($key, $array) ? $array[$key] : null;
    }
    
    public static function isRestRequest() {
        return defined('REST_REQUEST') && REST_REQUEST;
    }
    
    public static function arrayDot(array $array, $key, $default = null) {
        $keyParts = explode('.', $key);
        $result = $array;
        
        for ($i = 0; $i < count($keyParts); $i++) {
            
            $keyName = $keyParts[$i];
            
            if ($i === count($keyParts)-1) {
                return is_array($result) && array_key_exists($keyName, $result) ? $result[$keyName] : $default;
            }
            
            if (!is_array($result) || !array_key_exists($keyName, $result)) {
                return $default;
            }
            
            $result = $result[$keyName];
            
        }
        
        return $default;
    }
    
    public static function isEmail($email) {
        return is_email($email) || filter_var($email, FILTER_VALIDATE_EMAIL);
    }
    
    public static function phoneFormat(?string $phoneNumber, bool $digitsOnly = true) {
        if (empty($phoneNumber)) {
            return null;
        }
        
        $digits = static::getDigits($phoneNumber);
        $tenDigits = str_pad($digits, 10, "0", STR_PAD_LEFT);
        if ($digitsOnly) {
            return $tenDigits;
        }
        
        return '(' . substr($tenDigits, 0, 3) . ') ' .
            substr($tenDigits, 3, 3) . '-' .
            substr($tenDigits, 6);
    }
    
    public static function ordinal(int $number) {
        $formatter = new \NumberFormatter('en_US', \NumberFormatter::ORDINAL);
        return $formatter->format($number);
    }
    
    public static function moneyFormat(?float $number) {
        if (empty($number)) {
            return null;
        }
        
        $formatter = new \NumberFormatter('en_US', \NumberFormatter::CURRENCY);
        return $formatter->formatCurrency($number, 'USD');
    }
    
    public static function enumToList($enum) {
        $reflector = new \ReflectionClass($enum);
        $values = array_values($reflector->getConstants());
        return array_combine($values, $values);
    }
    
    private static function getDigits(string $numberWithText) {
        $digits = "";
        $count = 0;
        for ($i=0; $i<strlen($numberWithText); $i++) {
            if (ctype_digit($numberWithText[$i])) {
                $digits[$count++] = $numberWithText[$i];
            }
        }
        
        return $digits;
    }
}
