<?php

namespace Dragon\Hooking;

use Dragon\Abstracts\PluginHooksAbstract;
use Dragon\FileSystem;

class FrontEndPluginHooks extends PluginHooksAbstract {
	public static function init() {
		static::$dragonActions = array_merge(static::$dragonActions, [
			'shutdown'	=> [
				[static::class, 'shutdown'],
			],
		]);
		
		static::$dragonFilters = array_merge(static::$dragonFilters, [
			'woocommerce_disable_admin_bar' => [
				'__return_false',
			],
		]);
		
		parent::init();
		
		FileSystem::loadScripts([
			'dragon-utils' => 'util.js',
		]);
		
		FileSystem::loadCss([
			'dragon-frontend' => 'frontend.css',
		]);
	}
}
