<?php

namespace Dragon\Hooking;

use Dragon\Abstracts\PluginHooksAbstract;
use Dragon\Log;

class Cron extends PluginHooksAbstract {
    const SCHEDULE_HOURLY = 'hourly';
    const SCHEDULE_DAILY = 'daily';
    const SCHEDULE_TWICE_DAILY = 'twicedaily';
    
    protected static array $schedules = [
        // 		'dragon_every_minute' => [
        // 			'interval' => 60,
        // 			'display' => 'Once Every Minute',
        // 		],
    ];
    
    protected static $actions = [
        // 		'dragon_roaster_piggy_poster' => [
        // 			[Dragon::class, 'fire'],
        // 		],
    ];
    
    protected static $hookSchedules = [
        // 		'dragon_roaster_piggy_poster' => 'dragon_every_minute',
    ];
    
    public static function init() {
        parent::init();
        
        add_filter('cron_schedules', [static::class, 'addSchedules']);
        foreach (static::$hookSchedules as $hookName => $schedule) {
            static::scheduleCron($hookName, $schedule);
        }
    }
    
    public static function deactivate() {
        foreach (static::$hookSchedules as $hookName => $schedule) {
            static::unscheduleCron($hookName);
        }
    }
    
    public static function scheduleCron($hook, $timing, $firstRun = null) {
        if (!wp_next_scheduled($hook)) {
            
            $firstRun = empty($firstRun) ? (time()+5) : $firstRun;
            $response = wp_schedule_event($firstRun, $timing, $hook, [], true);
            
            if ($response instanceOf \WP_Error) {
                (new Log())->info('Scheduling Cron: ' . $hook);
                (new Log())->info($response->get_error_messages());
            }
            
        }
    }
    
    public static function unscheduleCron($hook) {
        wp_clear_scheduled_hook($hook);
    }
    
    public static function addSchedules() {
        return static::$schedules;
    }
}
