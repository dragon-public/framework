<?php

namespace Dragon\Hooking;

use Dragon\Abstracts\PluginHooksAbstract;
use Dragon\Config;
use Dragon\DB;
use Dragon\FileSystem;

class AdminPluginHooks extends PluginHooksAbstract {
    public static function init() {
        $providers = Config::providers();
        static::$dragonActions = array_merge(static::$dragonActions, [
            'admin_init'	=> [
                [$providers['on_demand']['AdminInit'], 'handleDownloads'],
                [$providers['on_demand']['AdminMenu'], 'removeMenus'],
            ],
            'admin_menu'	=> [
                [$providers['on_demand']['AdminMenu'], 'constructMenus'],
            ],
            'shutdown'	=> [
                [static::class, 'shutdown'],
            ],
        ]);
        
        static::$dragonFilters = array_merge(static::$dragonFilters, [
            'ajax_query_attachments_args'	=> [
                [$providers['global']['Role'], 'restrictMediaAccess'],
            ],
            'woocommerce_prevent_admin_access' => [
                '__return_false',
            ],
        ]);
        
        static::addPluginPageLinks();
        parent::init();
        
        FileSystem::loadCss([
            'dragon-css'	=> 'dragon-admin.css',
            'dragon-tokenfield'	=> 'tokenfield.css',
        ]);
        
        FileSystem::loadScripts([
            'dragon-utils' => 'util.js',
            'dragon-media' => 'media.js',
            'dragon-tokenfield' => 'tokenfield.min.js',
        ]);
        
        FileSystem::loadScriptUrls([
            'dragon-sortablejs' => 'https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js',
        ]);
    }
    
    public static function onActivation() {
        static::migrate();
    }
    
    public static function onDeactivation() {
        static::maybeRemoveTables();
    }
    
    public static function handlePluginPageLinks(array $links) {
        $pageLinks = static::getPluginPageLinks();
        foreach ($pageLinks as $name => $linkData) {
            $link = $linkData;
            $newTab = false;
            if (is_array($linkData)) {
                $link = $linkData['link'];
                if (array_key_exists('newTab', $linkData)) {
                    $newTab = (bool)$linkData['newTab'];
                }
            }
            
            $newTab = $newTab ? ' target="_blank"' : null;
            array_unshift($links, '<a href="' . $link . '"' . $newTab . '>' . $name . '</a>');
        }
        
        return $links;
    }
    
    protected static function migrate() {
        $db = DB::make();
        $db->migrate();
    }
    
    protected static function maybeRemoveTables() {
        if (get_option(Config::$namespace . '_remove_tables_on_deactivation', 'no') === 'yes') {
            
            $db = DB::make();
            $db->rollback();
            
        }
    }
    
    protected static function getPluginPageLinks() {
        return [];
    }
    
    private static function addPluginPageLinks() {
        $plugin = plugin_basename(Config::$pluginLoaderFile);
        add_filter("plugin_action_links_" . $plugin, [static::class, 'handlePluginPageLinks']);
    }
}
