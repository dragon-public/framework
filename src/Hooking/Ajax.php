<?php

namespace Dragon\Hooking;

use Dragon\Config;
use Dragon\Image;
use Dragon\Input;
use Dragon\Email\EmailMessageParser;
use Dragon\Models\EmailMessage;

class Ajax {
    protected static ?Input $input = null;
    protected static array $hooks = [
        // 		'hook_name_no_ns' => [
        // 			'callback' => [Ajax::class, 'method'],
        // 			'includeNoPrivs' => true,
        // 		],
    ];
    
    public static function init() {
        static::$input = new Input();
        
        static::createHook(
            'set_table_sorting',
            [Ajax::class, 'callActionForTableSorting'],
            false,
            true
            );
        
        static::createHook(
            'get_email_template',
            [Ajax::class, 'getEmailTemplate'],
            false,
            true
            );
        
        static::createHook(
            'get_media_image_url',
            [Ajax::class, 'getMediaImageUrl'],
            false,
            true
            );
        
        static::createHooks();
    }
    
    public static function getMediaImageUrl() {
        $id = static::$input->post('media_id');
        $size = static::$input->post('media_size', 'thumbnail');
        $data = Image::getMediaImageUrlById($id, $size);
        return [
            'url' => empty($data) ? false : $data,
        ];
    }
    
    public static function getEmailTemplate() {
        $emailTemplate = static::$input->post('email_template');
        $emailId = static::$input->post('email_id');
        $data = null;
        if (!empty($emailId)) {
            $emailMessage = EmailMessage::find($emailId);
            $data = $emailMessage->message;
        }
        
        return [
            'template' => (string)EmailMessageParser::makeTemplateFor($emailTemplate, $data),
        ];
    }
    
    public static function callActionForTableSorting() {
        $page = static::$input->post('page_slug');
        $order = static::$input->post('order');
        if (empty($page) || empty($order)) {
            return false;
        }
        
        do_action('dragon_admin_table_sorting_changed', $page, $order);
    }
    
    private static function createHooks() {
        foreach (static::$hooks as $hookName => $details) {
            $details['includeNoPrivs'] = !empty($details['includeNoPrivs']);
            static::createHook($hookName, $details['callback'], $details['includeNoPrivs']);
        }
    }
    
    private static function createHook(
        $hook,
        Callable $callback,
        $includeNoPrivledges = false,
        bool $dragonNs = false
        ) {
            $hookName = $dragonNs ? 'dragon_' . $hook : Config::$namespace . '_' . $hook;
            
            add_action('wp_ajax_' . $hookName, function () use ($callback) {
                static::doCallback($callback);
            });
                
                if ($includeNoPrivledges) {
                    add_action('wp_ajax_nopriv_' . $hookName, function () use ($callback) {
                        static::doCallback($callback);
                    });
                }
    }
    
    private static function doCallback(Callable $callback) {
        $response = $callback();
        if (is_array($response) || is_object($response)) {
            echo json_encode($response);
        } else {
            echo '{}';
        }
        
        wp_die();
    }
}
