<?php

namespace Dragon;

class Url {
	public static function getBySlug($page, array $additionalQuery = []) {
		$page = get_page_by_path($page);
		$url = get_permalink($page);
		
		if (!empty($additionalQuery)) {
			$url = static::changeQuery($url, $additionalQuery);
		}
		
		return $url;
	}
	
	public static function getProductBySlug($page) {
		$page = get_page_by_path($page, OBJECT, 'product');
		return get_permalink($page);
	}
	
	public static function redirect(string $link, string $displayText = "Let's change pages.") {
		echo Blade::view('redirect', ['next' => $link, 'display_text' => $displayText]);
		exit;
	}
	
	public static function getAdminMenuLink($slug, array $additionalQuery = []) {
		$link = menu_page_url($slug, false);
		if (empty($link)) {
			$link = static::getDomain() . '/wp-admin/admin.php?page=' . $slug;
		}
		
		$append = [];
		if (!empty($additionalQuery)) {
			foreach ($additionalQuery as $key => $val) {
				$append[] = $key . '=' . $val;
			}
		}
		
		$finished = empty($append) ? '' : '&' . implode('&', $append);
		return $link . $finished;
	}
	
	public static function getCurrentUrl(array $appendedQuery = [], array $skipVars = []) {
		$url = static::getDomain() . $_SERVER['REQUEST_URI'];
		return static::changeQuery($url, $appendedQuery, $skipVars);
	}
	
	public static function getDomain() {
		$protocol = empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] !== "on" ? "http" : "https";
		
		return $protocol . "://" .$_SERVER['HTTP_HOST'];
	}
	
	public static function image(string $path) {
		return static::resource('/img/' . $path);
	}
	
	public static function resource(string $path) {
		return Config::$pluginBaseUrl . '/resources/' . $path;
	}
	
	private static function changeQuery(string $url, array $appendedQuery, array $skipVars = []) {
		if (array_key_exists(0, $skipVars)) {
			$skipVars = array_combine(array_values($skipVars), array_values($skipVars));
		}
		
		$newQuery = static::parseQuery($url, array_merge($appendedQuery, $skipVars));
		
		foreach ($appendedQuery as $key => $val) {
			$newQuery[] = $key . '=' . $val;
		}
		
		$newQuery = implode('&', $newQuery);
		$parts = explode('?', $url);
		$parts[1] = $newQuery;
		return implode('?', $parts);
	}
	
	private static function parseQuery(string $url, array $skipVars) {
		$newQuery = [];
		$parsedQuery = parse_url($url, PHP_URL_QUERY);
		if (empty($parsedQuery)) {
			return [];
		}
		$query = explode('&', $parsedQuery);
		foreach ($query as $item) {
			list($key, $val) = explode('=', $item);
			if (array_key_exists($key, $skipVars)) {
				continue;
			}
			
			$newQuery[] = $key . '=' . $val;
		}
		
		return $newQuery;
	}
}
